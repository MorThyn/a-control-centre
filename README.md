# A Control Centre for ASUS ROG Laptops

This compact system tray application enables you to configure performance and GPU settings on your laptop, offering similar functionalities to ASUS Armory Crate but operates independently.

It's tailored for the Asus Zephyrus G14 2022 model, featuring AMD Radeon integrated and discrete GPUs. However, it may also be compatible with other G14 versions, G15, X FLOW, and various ROG series laptops.

## Performance Profile Switching

These profiles mirror those found in Armory Crate, complete with the original fan curves:

1. **Silent**: Keeps fans to a minimum or off, with a 70W total PPT and a maximum of 45W PPT allocated to the CPU.
2. **Balanced**: Moderates fan speeds, allowing for a 100W total PPT, with up to 45W PPT directed to the CPU.
3. **Turbo**: Ramps up fan intensity for a 125W total PPT, with up to 80W PPT available for the CPU.

## GPU Mode Switching

- **Eco Mode**: Activates only the low-power integrated GPU.
- **Standard Mode** (Windows Hybrid): Enables both the integrated and discrete GPUs.
- **Ultimate Mode**: Both GPUs are active, with the discrete GPU managing the built-in display.

## Additional Features

- Control over the keyboard backlight, including basic aura modes and colors.
- Set a **maximum battery charge limit** (60%, 80%, 100%) to extend battery lifespan.
- Monitor relative CPU and GPU fan speeds.
- Automatically switches between Standard and Eco GPU modes based on the laptop's power connection.
- Options to switch screen overdrive.
- Toggle CPU turbo boost.
- Monitor CPU and dGPU temperatures in Celsius and battery charge/discharge rates in Watts.

## Pending Features

- Anime Matrix control is not yet implemented as my model doesn't include it.
- Nvidia graphics support
- Custom Fan Profiles: Tailor fan speeds for any selected mode.
- Custom CPU and GPU power limits
- CPU Temp limit
- Use the FN+F5 and M4 (Rog) keys to cycle through Performance modes.
- Monitoring iGPU temperature
- Battery charge limit text input
- Battery charge limit UI steps