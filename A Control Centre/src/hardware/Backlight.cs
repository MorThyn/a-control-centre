﻿using HidLibrary;

namespace A_Control_Centre.hardware;

/// <summary>
/// Class responsible for handling the Backlight lighting modes and communication with HID devices.
/// </summary>
public static class Backlight
{
    private static readonly byte[] MessageSet = { 0x5d, 0xb5 };
    private static readonly byte[] MessageApply = { 0x5d, 0xb4 };

    public enum Modes
    {
        Static = 0,
        Breathe = 1,
        Strobe = 2,
        Rainbow = 3,
        Custom = 10 // Assumed custom mode.
    }

    private enum Speeds
    {
        Slow = 0,
        Medium = 1,
        High = 2
    }

    public static Modes Mode { get; set; } = Modes.Static;
    public static Color Color1 { get; set; } = Color.White;
    private static Color Color2 { get; set; } = Color.Black;
    private static Speeds Speed { get; set; } = Speeds.Slow;

    /// <summary>
    /// Constructs the message to set the Backlight lighting mode.
    /// </summary>
    /// <param name="mode">The lighting mode.</param>
    /// <param name="color">The primary color.</param>
    /// <param name="color2">The secondary color.</param>
    /// <param name="speed">The speed of the effect.</param>
    /// <returns>A byte array representing the message to send to the device.</returns>
    private static byte[] ConstructBacklightMessage(Modes mode, Color color, Color color2, Speeds speed)
    {
        byte[] msg = new byte[17];
        msg[0] = 0x5d;
        msg[1] = 0xb3;
        msg[2] = 0x00; // Zone
        msg[3] = (byte)mode; // Backlight Mode
        msg[4] = color.R; // Red component
        msg[5] = color.G; // Green component
        msg[6] = color.B; // Blue component
        msg[7] = (byte)speed; // Speed
        // msg[8] and msg[9] appear to be unused or reserved
        msg[10] = color2.R; // Secondary Red component
        msg[11] = color2.G; // Secondary Green component
        msg[12] = color2.B; // Secondary Blue component
        return msg;
    }

    /// <summary>
    /// Applies the current Backlight settings to the connected HID devices.
    /// </summary>
    public static void ApplyBacklight()
    {
        var deviceIds = new[] { 0x1854, 0x1869, 0x1866, 0x19b6 };
        var hidDeviceList = HidDevices.Enumerate(0x0b05, deviceIds)
            .Where(d => d.IsConnected && d.Description.Contains("HID")).ToList();

        foreach (var device in hidDeviceList)
        {
            device.OpenDevice();
            byte[] msg = ConstructBacklightMessage(Mode, Color1, Color2, Speed);
            device.Write(msg);
            device.Write(MessageSet);
            device.Write(MessageApply);
            device.CloseDevice();
        }
    }
}