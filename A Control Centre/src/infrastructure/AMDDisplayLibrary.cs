﻿using System.Diagnostics;
using System.Runtime.InteropServices;

namespace A_Control_Centre.infrastructure;

#region Export Struct

/// <summary>
/// Represents single sensor data, including support status and value.
/// </summary>
[StructLayout(LayoutKind.Sequential)]
public struct ADLSingleSensorData
{
    /// <summary>
    /// Indicates if the sensor is supported.
    /// </summary>
    public int Supported;

    /// <summary>
    /// The current value of the sensor.
    /// </summary>
    public int Value;
}

/// <summary>
/// Represents the output data for power management logging, including size and sensors data.
/// </summary>
[StructLayout(LayoutKind.Sequential)]
public struct ADLPMLogDataOutput
{
    private int Size;

    /// <summary>
    /// Array of sensor data.
    /// </summary>
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = AMDDisplayLibrary.ADL_PMLOG_MAX_SENSORS)]
    public ADLSingleSensorData[] Sensors;
}

/// <summary>
/// Contains information about the Graphics Core Next (GCN) architecture, including compute units, texture mapping units, and ASIC identifiers.
/// </summary>
[StructLayout(LayoutKind.Sequential)]
public struct ADLGcnInfo
{
    /// <summary>
    /// Number of compute units on the ASIC.
    /// </summary>
    public int CuCount;

    /// <summary>
    /// Number of texture mapping units.
    /// </summary>
    public int TexCount;

    /// <summary>
    /// Number of Render backend Units.
    /// </summary>
    public int RopCount;

    /// <summary>
    /// Family ID of the ASIC, such as SI, VI. Refer to atiid.h for family IDs.
    /// </summary>
    public int ASICFamilyId;

    /// <summary>
    /// Revision ID of the ASIC, such as Ellesmere, Fiji. Refer to vi_id.h for VI family revision IDs.
    /// </summary>
    public int ASICRevisionId;
}

/// <summary>
/// Defines the types of ASIC families.
/// </summary>
[Flags]
public enum ADLAsicFamilyType
{
    Undefined = 0,
    Discrete = 1 << 0,
    Integrated = 1 << 1,
    Workstation = 1 << 2,
    FireMV = 1 << 3,
    Xgp = 1 << 4,
    Fusion = 1 << 5,
    Firestream = 1 << 6,
    Embedded = 1 << 7,
}

public enum ADLSensorType {
    SENSOR_MAXTYPES = 0,
    PMLOG_CLK_GFXCLK = 1, // Current graphic clock value in MHz
    PMLOG_CLK_MEMCLK = 2, // Current memory clock value in MHz
    PMLOG_CLK_SOCCLK = 3,
    PMLOG_CLK_UVDCLK1 = 4,
    PMLOG_CLK_UVDCLK2 = 5,
    PMLOG_CLK_VCECLK = 6,
    PMLOG_CLK_VCNCLK = 7,
    PMLOG_TEMPERATURE_EDGE = 8, // Current edge of the die temperature value in C
    PMLOG_TEMPERATURE_MEM = 9,
    PMLOG_TEMPERATURE_VRVDDC = 10,
    PMLOG_TEMPERATURE_VRMVDD = 11,
    PMLOG_TEMPERATURE_LIQUID = 12,
    PMLOG_TEMPERATURE_PLX = 13,
    PMLOG_FAN_RPM = 14, // Current fan RPM value
    PMLOG_FAN_PERCENTAGE = 15, // Current ratio of fan RPM and max RPM
    PMLOG_SOC_VOLTAGE = 16,
    PMLOG_SOC_POWER = 17,
    PMLOG_SOC_CURRENT = 18,
    PMLOG_INFO_ACTIVITY_GFX = 19, // Current graphic activity level in percentage
    PMLOG_INFO_ACTIVITY_MEM = 20, // Current memory activity level in percentage
    PMLOG_GFX_VOLTAGE = 21, // Current graphic voltage in mV
    PMLOG_MEM_VOLTAGE = 22,
    PMLOG_ASIC_POWER = 23, // Current ASIC power draw in Watt
    PMLOG_TEMPERATURE_VRSOC = 24,
    PMLOG_TEMPERATURE_VRMVDD0 = 25,
    PMLOG_TEMPERATURE_VRMVDD1 = 26,
    PMLOG_TEMPERATURE_HOTSPOT = 27, // Current center of the die temperature value in C
    PMLOG_TEMPERATURE_GFX = 28,
    PMLOG_TEMPERATURE_SOC = 29,
    PMLOG_GFX_POWER = 30,
    PMLOG_GFX_CURRENT = 31,
    PMLOG_TEMPERATURE_CPU = 32,
    PMLOG_CPU_POWER = 33,
    PMLOG_CLK_CPUCLK = 34,
    PMLOG_THROTTLER_STATUS = 35, // A bit map of GPU throttle information. If a bit is set, the bit represented type of thorttling occurred in the last metrics sampling period
    PMLOG_CLK_VCN1CLK1 = 36,
    PMLOG_CLK_VCN1CLK2 = 37,
    PMLOG_SMART_POWERSHIFT_CPU = 38,
    PMLOG_SMART_POWERSHIFT_DGPU = 39,
    PMLOG_BUS_SPEED = 40, // Current PCIE bus speed running
    PMLOG_BUS_LANES = 41, // Current PCIE bus lanes using
    PMLOG_TEMPERATURE_LIQUID0 = 42,
    PMLOG_TEMPERATURE_LIQUID1 = 43,
    PMLOG_CLK_FCLK = 44,
    PMLOG_THROTTLER_STATUS_CPU = 45,
    PMLOG_SSPAIRED_ASICPOWER = 46, // apuPower
    PMLOG_SSTOTAL_POWERLIMIT = 47, // Total Power limit    
    PMLOG_SSAPU_POWERLIMIT = 48, // APU Power limit
    PMLOG_SSDGPU_POWERLIMIT = 49, // DGPU Power limit
    PMLOG_TEMPERATURE_HOTSPOT_GCD = 50,
    PMLOG_TEMPERATURE_HOTSPOT_MCD = 51,
    PMLOG_THROTTLER_TEMP_EDGE_PERCENTAGE = 52,
    PMLOG_THROTTLER_TEMP_HOTSPOT_PERCENTAGE = 53,
    PMLOG_THROTTLER_TEMP_HOTSPOT_GCD_PERCENTAGE = 54,
    PMLOG_THROTTLER_TEMP_HOTSPOT_MCD_PERCENTAGE = 55,
    PMLOG_THROTTLER_TEMP_MEM_PERCENTAGE = 56,
    PMLOG_THROTTLER_TEMP_VR_GFX_PERCENTAGE = 57,
    PMLOG_THROTTLER_TEMP_VR_MEM0_PERCENTAGE = 58,
    PMLOG_THROTTLER_TEMP_VR_MEM1_PERCENTAGE = 59,
    PMLOG_THROTTLER_TEMP_VR_SOC_PERCENTAGE = 60,
    PMLOG_THROTTLER_TEMP_LIQUID0_PERCENTAGE = 61,
    PMLOG_THROTTLER_TEMP_LIQUID1_PERCENTAGE = 62,
    PMLOG_THROTTLER_TEMP_PLX_PERCENTAGE = 63,
    PMLOG_THROTTLER_TDC_GFX_PERCENTAGE = 64,
    PMLOG_THROTTLER_TDC_SOC_PERCENTAGE = 65,
    PMLOG_THROTTLER_TDC_USR_PERCENTAGE = 66,
    PMLOG_THROTTLER_PPT0_PERCENTAGE = 67,
    PMLOG_THROTTLER_PPT1_PERCENTAGE = 68,
    PMLOG_THROTTLER_PPT2_PERCENTAGE = 69,
    PMLOG_THROTTLER_PPT3_PERCENTAGE = 70,
    PMLOG_THROTTLER_FIT_PERCENTAGE = 71,
    PMLOG_THROTTLER_GFX_APCC_PLUS_PERCENTAGE = 72,
    PMLOG_BOARD_POWER = 73,
    PMLOG_MAX_SENSORS_REAL
};

//Throttle Status
[Flags]
public enum ADL_THROTTLE_NOTIFICATION {
    ADL_PMLOG_THROTTLE_POWER = 1 << 0,
    ADL_PMLOG_THROTTLE_THERMAL = 1 << 1,
    ADL_PMLOG_THROTTLE_CURRENT = 1 << 2,
};

public enum ADL_PMLOG_SENSORS {
    ADL_SENSOR_MAXTYPES = 0,
    ADL_PMLOG_CLK_GFXCLK = 1,
    ADL_PMLOG_CLK_MEMCLK = 2,
    ADL_PMLOG_CLK_SOCCLK = 3,
    ADL_PMLOG_CLK_UVDCLK1 = 4,
    ADL_PMLOG_CLK_UVDCLK2 = 5,
    ADL_PMLOG_CLK_VCECLK = 6,
    ADL_PMLOG_CLK_VCNCLK = 7,
    ADL_PMLOG_TEMPERATURE_EDGE = 8,
    ADL_PMLOG_TEMPERATURE_MEM = 9,
    ADL_PMLOG_TEMPERATURE_VRVDDC = 10,
    ADL_PMLOG_TEMPERATURE_VRMVDD = 11,
    ADL_PMLOG_TEMPERATURE_LIQUID = 12,
    ADL_PMLOG_TEMPERATURE_PLX = 13,
    ADL_PMLOG_FAN_RPM = 14,
    ADL_PMLOG_FAN_PERCENTAGE = 15,
    ADL_PMLOG_SOC_VOLTAGE = 16,
    ADL_PMLOG_SOC_POWER = 17,
    ADL_PMLOG_SOC_CURRENT = 18,
    ADL_PMLOG_INFO_ACTIVITY_GFX = 19,
    ADL_PMLOG_INFO_ACTIVITY_MEM = 20,
    ADL_PMLOG_GFX_VOLTAGE = 21,
    ADL_PMLOG_MEM_VOLTAGE = 22,
    ADL_PMLOG_ASIC_POWER = 23,
    ADL_PMLOG_TEMPERATURE_VRSOC = 24,
    ADL_PMLOG_TEMPERATURE_VRMVDD0 = 25,
    ADL_PMLOG_TEMPERATURE_VRMVDD1 = 26,
    ADL_PMLOG_TEMPERATURE_HOTSPOT = 27,
    ADL_PMLOG_TEMPERATURE_GFX = 28,
    ADL_PMLOG_TEMPERATURE_SOC = 29,
    ADL_PMLOG_GFX_POWER = 30,
    ADL_PMLOG_GFX_CURRENT = 31,
    ADL_PMLOG_TEMPERATURE_CPU = 32,
    ADL_PMLOG_CPU_POWER = 33,
    ADL_PMLOG_CLK_CPUCLK = 34,
    ADL_PMLOG_THROTTLER_STATUS = 35, // GFX
    ADL_PMLOG_CLK_VCN1CLK1 = 36,
    ADL_PMLOG_CLK_VCN1CLK2 = 37,
    ADL_PMLOG_SMART_POWERSHIFT_CPU = 38,
    ADL_PMLOG_SMART_POWERSHIFT_DGPU = 39,
    ADL_PMLOG_BUS_SPEED = 40,
    ADL_PMLOG_BUS_LANES = 41,
    ADL_PMLOG_TEMPERATURE_LIQUID0 = 42,
    ADL_PMLOG_TEMPERATURE_LIQUID1 = 43,
    ADL_PMLOG_CLK_FCLK = 44,
    ADL_PMLOG_THROTTLER_STATUS_CPU = 45,
    ADL_PMLOG_SSPAIRED_ASICPOWER = 46, // apuPower
    ADL_PMLOG_SSTOTAL_POWERLIMIT = 47, // Total Power limit
    ADL_PMLOG_SSAPU_POWERLIMIT = 48, // APU Power limit
    ADL_PMLOG_SSDGPU_POWERLIMIT = 49, // DGPU Power limit
    ADL_PMLOG_TEMPERATURE_HOTSPOT_GCD = 50,
    ADL_PMLOG_TEMPERATURE_HOTSPOT_MCD = 51,
    ADL_PMLOG_THROTTLER_TEMP_EDGE_PERCENTAGE = 52,
    ADL_PMLOG_THROTTLER_TEMP_HOTSPOT_PERCENTAGE = 53,
    ADL_PMLOG_THROTTLER_TEMP_HOTSPOT_GCD_PERCENTAGE = 54,
    ADL_PMLOG_THROTTLER_TEMP_HOTSPOT_MCD_PERCENTAGE = 55,
    ADL_PMLOG_THROTTLER_TEMP_MEM_PERCENTAGE = 56,
    ADL_PMLOG_THROTTLER_TEMP_VR_GFX_PERCENTAGE = 57,
    ADL_PMLOG_THROTTLER_TEMP_VR_MEM0_PERCENTAGE = 58,
    ADL_PMLOG_THROTTLER_TEMP_VR_MEM1_PERCENTAGE = 59,
    ADL_PMLOG_THROTTLER_TEMP_VR_SOC_PERCENTAGE = 60,
    ADL_PMLOG_THROTTLER_TEMP_LIQUID0_PERCENTAGE = 61,
    ADL_PMLOG_THROTTLER_TEMP_LIQUID1_PERCENTAGE = 62,
    ADL_PMLOG_THROTTLER_TEMP_PLX_PERCENTAGE = 63,
    ADL_PMLOG_THROTTLER_TDC_GFX_PERCENTAGE = 64,
    ADL_PMLOG_THROTTLER_TDC_SOC_PERCENTAGE = 65,
    ADL_PMLOG_THROTTLER_TDC_USR_PERCENTAGE = 66,
    ADL_PMLOG_THROTTLER_PPT0_PERCENTAGE = 67,
    ADL_PMLOG_THROTTLER_PPT1_PERCENTAGE = 68,
    ADL_PMLOG_THROTTLER_PPT2_PERCENTAGE = 69,
    ADL_PMLOG_THROTTLER_PPT3_PERCENTAGE = 70,
    ADL_PMLOG_THROTTLER_FIT_PERCENTAGE = 71,
    ADL_PMLOG_THROTTLER_GFX_APCC_PLUS_PERCENTAGE = 72,
    ADL_PMLOG_BOARD_POWER = 73,
    ADL_PMLOG_MAX_SENSORS_REAL
}

#region ADLAdapterInfo

/// <summary>
/// Contains information about an adapter, including ID, bus number, vendor ID, and device names.
/// </summary>
[StructLayout(LayoutKind.Sequential)]
public struct ADLAdapterInfo
{
    private int Size;

    /// <summary>
    /// Adapter index.
    /// </summary>
    public int AdapterIndex;

    /// <summary>
    /// Unique Device Identifier (UDID) of the adapter.
    /// </summary>
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = AMDDisplayLibrary.ADL_MAX_PATH)]
    public string UDID;

    /// <summary>
    /// Bus number of the adapter.
    /// </summary>
    public int BusNumber;

    /// <summary>
    /// Driver number.
    /// </summary>
    public int DriverNumber;

    /// <summary>
    /// Function number.
    /// </summary>
    public int FunctionNumber;

    /// <summary>
    /// Vendor ID of the adapter.
    /// </summary>
    public int VendorID;

    /// <summary>
    /// Name of the adapter.
    /// </summary>
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = AMDDisplayLibrary.ADL_MAX_PATH)]
    public string AdapterName;

    /// <summary>
    /// Display name of the adapter.
    /// </summary>
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = AMDDisplayLibrary.ADL_MAX_PATH)]
    public string DisplayName;

    /// <summary>
    /// Indicates whether the adapter is present.
    /// </summary>
    public int Present;

    /// <summary>
    /// Indicates whether the adapter exists.
    /// </summary>
    public int Exist;

    /// <summary>
    /// Path to the adapter driver.
    /// </summary>
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = AMDDisplayLibrary.ADL_MAX_PATH)]
    public string DriverPath;

    /// <summary>
    /// Extension path for the adapter driver.
    /// </summary>
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = AMDDisplayLibrary.ADL_MAX_PATH)]
    public string DriverPathExt;

    /// <summary>
    /// Plug and Play (PNP) string of the adapter.
    /// </summary>
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = AMDDisplayLibrary.ADL_MAX_PATH)]
    public string PNPString;

    /// <summary>
    /// OS display index.
    /// </summary>
    public int OSDisplayIndex;
}

/// <summary>
/// Represents an array of <see cref="ADLAdapterInfo"/> structures.
/// </summary>
[StructLayout(LayoutKind.Sequential)]
public struct ADLAdapterInfoArray
{
    /// <summary>
    /// Array of adapter information structures.
    /// </summary>
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = AMDDisplayLibrary.ADL_MAX_ADAPTERS)]
    public ADLAdapterInfo[] ADLAdapterInfo;
}

#endregion ADLAdapterInfo

#region ADLDisplayInfo

// The ADLDisplayID and ADLDisplayInfo structures can be documented in a similar manner, 
// providing details on each field and its significance.

#endregion ADLDisplayInfo

#endregion Export Struct

/// <summary>
/// Provides access to the AMD display library via P/Invoke interop to "atiadlxx.dll".
/// </summary>
public static class AMDDisplayLibrary
{
    private const string Atiadlxx_FileName = "atiadlxx.dll";

    #region Internal Constants

    /// <summary>Maximum path length.</summary>
    public const int ADL_MAX_PATH = 256;

    /// <summary>Maximum number of adapters.</summary>
    public const int ADL_MAX_ADAPTERS = 40;

    /// <summary>Maximum number of displays.</summary>
    public const int ADL_MAX_DISPLAYS = 40;

    /// <summary>Maximum device name length.</summary>
    public const int ADL_MAX_DEVICENAME = 32;

    /// <summary>Indicates a successful operation.</summary>
    public const int ADL_SUCCESS = 0;

    /// <summary>Indicates a failed operation.</summary>
    public const int ADL_FAIL = -1;

    /// <summary>Driver operation succeeded.</summary>
    public const int ADL_DRIVER_OK = 0;

    /// <summary>Maximum number of GL-Sync ports on the GL-Sync module.</summary>
    public const int ADL_MAX_GLSYNC_PORTS = 8;

    /// <summary>Maximum number of GL-Sync port LEDs.</summary>
    public const int ADL_MAX_GLSYNC_PORT_LEDS = 8;

    /// <summary>Maximum number of display modes for the adapter.</summary>
    public const int ADL_MAX_NUM_DISPLAYMODES = 1024;

    /// <summary>Maximum number of sensors for performance metrics logging.</summary>
    public const int ADL_PMLOG_MAX_SENSORS = 256;

    #endregion Internal Constants

    private static bool? isDllLoaded;

    private static readonly AMDNativeMethods.ADL_Main_Memory_Alloc
        ADL_Main_Memory_Alloc_Impl_Reference = Memory_Alloc_Impl;

    /// <summary>
    /// Attempts to load the "atiadlxx.dll" library and prelink all methods.
    /// </summary>
    /// <returns>true if the library is loaded successfully; otherwise, false.</returns>
    public static bool Load()
    {
        if (isDllLoaded != null)
        {
            return isDllLoaded.Value;
        }

        try
        {
            Marshal.PrelinkAll(typeof(AMDDisplayLibrary));
            isDllLoaded = true;
        }
        catch (Exception e) when (e is DllNotFoundException or EntryPointNotFoundException)
        {
            Debug.WriteLine(e);
            isDllLoaded = false;
        }

        return isDllLoaded.Value;
    }

    /// <summary>
    /// Creates the ADL context.
    /// </summary>
    /// <param name="enumConnectedAdapters">If set to 1, ADL will only return physically existing adapters.</param>
    /// <param name="adlContextHandle">Output parameter for ADL context handle.</param>
    /// <returns>ADL error code.</returns>
    public static int ADL2_Main_Control_Create(int enumConnectedAdapters, out IntPtr adlContextHandle)
    {
        return AMDNativeMethods.ADL2_Main_Control_Create(ADL_Main_Memory_Alloc_Impl_Reference, enumConnectedAdapters,
            out adlContextHandle);
    }

    /// <summary>
    /// Frees memory allocated by ADL.
    /// </summary>
    /// <param name="buffer">Pointer to the memory buffer to be freed.</param>
    public static void FreeMemory(IntPtr buffer)
    {
        Memory_Free_Impl(buffer);
    }

    /// <summary>
    /// Built-in memory allocation function.
    /// </summary>
    /// <param name="size">Size of the memory to allocate.</param>
    /// <returns>Pointer to the allocated memory.</returns>
    private static IntPtr Memory_Alloc_Impl(int size)
    {
        return Marshal.AllocCoTaskMem(size);
    }

    /// <summary>
    /// Built-in memory free function.
    /// </summary>
    /// <param name="buffer">Pointer to the memory buffer to be freed.</param>
    private static void Memory_Free_Impl(IntPtr buffer)
    {
        if (IntPtr.Zero != buffer)
        {
            Marshal.FreeCoTaskMem(buffer);
        }
    }

    /// <summary>
    /// Contains P/Invoke declarations for interacting with the underlying "atiadlxx.dll" library.
    /// </summary>
    public static class AMDNativeMethods
    {
        /// <summary>
        /// Delegate for memory allocation callback used by ADL.
        /// </summary>
        /// <param name="size">Size of the memory to allocate.</param>
        /// <returns>Pointer to the allocated memory.</returns>
        public delegate IntPtr ADL_Main_Memory_Alloc(int size);

        [DllImport(Atiadlxx_FileName)]
        public static extern int ADL2_Main_Control_Create(ADL_Main_Memory_Alloc callback, int enumConnectedAdapters,
            out IntPtr adlContextHandle);

        [DllImport(Atiadlxx_FileName)]
        public static extern int ADL2_Main_Control_Destroy(IntPtr adlContextHandle);

        [DllImport(Atiadlxx_FileName)]
        public static extern int ADL2_Adapter_NumberOfAdapters_Get(IntPtr adlContextHandle, out int numAdapters);

        [DllImport(Atiadlxx_FileName)]
        public static extern int ADL2_Adapter_AdapterInfo_Get(IntPtr adlContextHandle, IntPtr info, int inputSize);

        [DllImport(Atiadlxx_FileName)]
        public static extern int ADL2_Adapter_Active_Get(IntPtr adlContextHandle, int adapterIndex, out int status);

        [DllImport(Atiadlxx_FileName)]
        public static extern int ADL2_Display_DisplayInfo_Get(IntPtr adlContextHandle, int adapterIndex,
            out int numDisplays, out IntPtr displayInfoArray, int forceDetect);

        [DllImport(Atiadlxx_FileName)]
        public static extern int ADL2_Overdrive_Caps(IntPtr adlContextHandle, int adapterIndex, out int supported,
            out int enabled, out int version);

        [DllImport(Atiadlxx_FileName)]
        public static extern int ADL2_New_QueryPMLogData_Get(IntPtr adlContextHandle, int adapterIndex,
            out ADLPMLogDataOutput adlpmLogDataOutput);

        [DllImport(Atiadlxx_FileName)]
        public static extern int ADL2_Adapter_ASICFamilyType_Get(IntPtr adlContextHandle, int adapterIndex,
            out ADLAsicFamilyType asicFamilyType, out int asicFamilyTypeValids);
    }
}