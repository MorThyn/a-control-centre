﻿using System.Diagnostics;
using System.Runtime.InteropServices;
using A_Control_Centre.utilities;

namespace A_Control_Centre.infrastructure;

public static class NativeMethods
{
    // User32.dll functions
    [DllImport("user32.dll", SetLastError = true)]
    private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

    private const int SW_RESTORE = 9;

    // PowrProf.dll functions for power management

    [DllImport("PowrProf.dll", CharSet = CharSet.Unicode, SetLastError = true)]
    private static extern uint PowerWriteDCValueIndex(IntPtr RootPowerKey,
        [MarshalAs(UnmanagedType.LPStruct)] Guid SchemeGuid,
        [MarshalAs(UnmanagedType.LPStruct)] Guid SubGroupOfPowerSettingsGuid,
        [MarshalAs(UnmanagedType.LPStruct)] Guid PowerSettingGuid,
        int DcValueIndex);

    [DllImport("PowrProf.dll", CharSet = CharSet.Unicode, SetLastError = true)]
    private static extern uint PowerWriteACValueIndex(IntPtr RootPowerKey,
        [MarshalAs(UnmanagedType.LPStruct)] Guid SchemeGuid,
        [MarshalAs(UnmanagedType.LPStruct)] Guid SubGroupOfPowerSettingsGuid,
        [MarshalAs(UnmanagedType.LPStruct)] Guid PowerSettingGuid,
        int AcValueIndex);

    [DllImport("PowrProf.dll", CharSet = CharSet.Unicode, SetLastError = true)]
    private static extern uint PowerReadACValueIndex(IntPtr RootPowerKey,
        [MarshalAs(UnmanagedType.LPStruct)] Guid SchemeGuid,
        [MarshalAs(UnmanagedType.LPStruct)] Guid SubGroupOfPowerSettingsGuid,
        [MarshalAs(UnmanagedType.LPStruct)] Guid PowerSettingGuid,
        out IntPtr AcValueIndex);

    [DllImport("PowrProf.dll", CharSet = CharSet.Unicode, SetLastError = true)]
    private static extern uint PowerSetActiveScheme(IntPtr RootPowerKey,
        [MarshalAs(UnmanagedType.LPStruct)] Guid SchemeGuid);

    [DllImport("PowrProf.dll", CharSet = CharSet.Unicode, SetLastError = true)]
    private static extern uint PowerGetActiveScheme(IntPtr UserPowerKey, out IntPtr ActivePolicyGuid);

    // GUIDs for CPU and Power Boost Mode
    static readonly Guid GUID_CPU = new Guid("54533251-82be-4824-96c1-47b60b740d00");
    static readonly Guid GUID_BOOST = new Guid("be337238-0d82-4146-a960-4f3749d470c7");

    /// <summary>
    /// Represents the DEVMODE data structure that contains device-specific settings for a display device.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    private struct DEVMODE
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string dmDeviceName;

        public short dmSpecVersion;
        public short dmDriverVersion;
        public short dmSize;
        public short dmDriverExtra;
        public int dmFields;
        public int dmPositionX;
        public int dmPositionY;
        public int dmDisplayOrientation;
        public int dmDisplayFixedOutput;
        public short dmColor;
        public short dmDuplex;
        public short dmYResolution;
        public short dmTTOption;
        public short dmCollate;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string dmFormName;

        public short dmLogPixels;
        public short dmBitsPerPel;
        public int dmPelsWidth;
        public int dmPelsHeight;
        public int dmDisplayFlags;
        public int dmDisplayFrequency;
        public int dmICMMethod;
        public int dmICMIntent;
        public int dmMediaType;
        public int dmDitherType;
        public int dmReserved1;
        public int dmReserved2;
        public int dmPanningWidth;
        public int dmPanningHeight;
    };

    /// <summary>
    /// Flags for ChangeDisplaySettingsEx function.
    /// </summary>
    [Flags]
    public enum DisplaySettingsFlags : int
    {
        CDS_UPDATEREGISTRY = 1,
        CDS_TEST = 2,
        CDS_FULLSCREEN = 4,
        CDS_GLOBAL = 8,
        CDS_SET_PRIMARY = 0x10,
        CDS_RESET = 0x40000000,
        CDS_NORESET = 0x10000000
    }

    // Constants for display settings
    private const int ENUM_CURRENT_SETTINGS = -1;
    private const string LaptopScreenName = "\\\\.\\DISPLAY1";

    /// <summary>
    /// Creates a new DEVMODE structure with initialized fields.
    /// </summary>
    /// <returns>A new instance of DEVMODE with default values.</returns>
    private static DEVMODE CreateDevmode()
    {
        DEVMODE dm = new DEVMODE
        {
            dmDeviceName = new string(new char[32]),
            dmFormName = new string(new char[32]),
            dmSize = (short)Marshal.SizeOf(typeof(DEVMODE))
        };
        return dm;
    }

    // P/Invoke declarations

    /// <summary>
    /// Enumerates display settings for a display device.
    /// </summary>
    [DllImport("user32.dll")]
    private static extern int EnumDisplaySettingsEx(string lpszDeviceName, int iModeNum, ref DEVMODE lpDevMode);

    /// <summary>
    /// Changes display settings for a display device.
    /// </summary>
    [DllImport("user32.dll")]
    private static extern int ChangeDisplaySettingsEx(string lpszDeviceName, ref DEVMODE lpDevMode, IntPtr hwnd,
        DisplaySettingsFlags dwflags, IntPtr lParam);

    // Retrieves the laptop's primary screen.
    private static Screen FindLaptopScreen()
    {
        return Screen.AllScreens.FirstOrDefault(screen => screen.DeviceName == LaptopScreenName)!;
    }

    // Gets the current refresh rate of the laptop's screen.
    public static int GetRefreshRate()
    {
        var laptopScreen = FindLaptopScreen();

        var dm = CreateDevmode();
        if (EnumDisplaySettingsEx(laptopScreen.DeviceName, ENUM_CURRENT_SETTINGS, ref dm) !=
            0)
        {
            return dm.dmDisplayFrequency;
        }

        return -1; // Return -1 if unable to retrieve display settings
    }

    // Sets the refresh rate of the laptop's screen.
    public static int SetRefreshRate(int frequency = 144)
    {
        var laptopScreen = FindLaptopScreen();

        var dm = CreateDevmode();
        if (EnumDisplaySettingsEx(laptopScreen.DeviceName, ENUM_CURRENT_SETTINGS, ref dm) ==
            0) return 0; // Indicate no change was made
        dm.dmDisplayFrequency = frequency;
        return ChangeDisplaySettingsEx(laptopScreen.DeviceName, ref dm, IntPtr.Zero,
            DisplaySettingsFlags.CDS_UPDATEREGISTRY, IntPtr.Zero);

    }

    // Gets the current active power scheme GUID.
    static Guid GetActiveScheme()
    {
        var hr = PowerGetActiveScheme(IntPtr.Zero, out var pActiveSchemeGuid);
        Guid activeSchemeGuid = (Guid)Marshal.PtrToStructure(pActiveSchemeGuid, typeof(Guid));
        return activeSchemeGuid;
    }

    // Retrieves the current CPU boost setting.
    public static int GetCPUBoost()
    {
        var activeSchemeGuid = GetActiveScheme();
        PowerReadACValueIndex(IntPtr.Zero, activeSchemeGuid, GUID_CPU,
            GUID_BOOST, out IntPtr acValueIndex);
        return acValueIndex.ToInt32();
    }

    // Sets the CPU boost setting.
    public static void SetCPUBoost(int boost = 0)
    {
        var activeSchemeGuid = GetActiveScheme();
        PowerWriteACValueIndex(IntPtr.Zero, activeSchemeGuid, GUID_CPU,
            GUID_BOOST, boost);
        PowerSetActiveScheme(IntPtr.Zero, activeSchemeGuid);
        PowerWriteDCValueIndex(IntPtr.Zero, activeSchemeGuid, GUID_CPU,
            GUID_BOOST, boost);
        PowerSetActiveScheme(IntPtr.Zero, activeSchemeGuid);
    }
}