﻿using System.Management;
using System.Runtime.InteropServices;

namespace A_Control_Centre.infrastructure;

/// <summary>
/// Provides functionality to interact with WMI (Windows Management Instrumentation) for hardware control.
/// </summary>
public class AsusWMI : IDisposable
{
    private const string FileName = @"\\.\\ATKACPI";
    private const uint ControlCode = 0x0022240C;

    private const uint DSTS = 0x53545344;
    private const uint DEVS = 0x53564544;

    public const uint CPUFan = 0x00110013;
    public const uint GPUFan = 0x00110014;
    public const uint PerformanceMode = 0x00120075; // Thermal Control

    public const uint GPUEco = 0x00090020;
    public const uint GPUMux = 0x00090016;

    public const uint BatteryLimit = 0x00120057;
    public const uint ScreenOverdrive = 0x00050019;

    public const int TempCPU = 0x00120094;
    public const int TempGPU = 0x00120097;


    public enum FanModes
    {
        Balanced = 0,
        Turbo = 1,
        Silent = 2,
    }

    public enum GPUModes
    {
        Eco = 0,
        Standard = 1,
        Ultimate = 2,
    }

    public static FanModes FanMode { get; set; } = FanModes.Balanced;
    public static GPUModes GPUMode { get; set; } = GPUModes.Standard;

    private IntPtr _handle;

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    private static extern IntPtr CreateFile(
        string lpFileName,
        uint dwDesiredAccess,
        uint dwShareMode,
        IntPtr lpSecurityAttributes,
        uint dwCreationDisposition,
        uint dwFlagsAndAttributes,
        IntPtr hTemplateFile);

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern bool DeviceIoControl(
        IntPtr hDevice,
        uint dwIoControlCode,
        byte[] lpInBuffer,
        uint nInBufferSize,
        byte[] lpOutBuffer,
        uint nOutBufferSize,
        ref uint lpBytesReturned,
        IntPtr lpOverlapped);

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern bool CloseHandle(IntPtr hObject);

    private const uint GENERIC_READ = 0x80000000;
    private const uint GENERIC_WRITE = 0x40000000;
    private const uint OPEN_EXISTING = 3;
    private const uint FILE_ATTRIBUTE_NORMAL = 0x80;
    private const uint FILE_SHARE_READ = 1;
    private const uint FILE_SHARE_WRITE = 2;

    /// <summary>
    /// Initializes a new instance of the ASUSWmi class by connecting to the ACPI device.
    /// </summary>
    public AsusWMI()
    {
        _handle = CreateFile(
            FileName,
            GENERIC_READ | GENERIC_WRITE,
            FILE_SHARE_READ | FILE_SHARE_WRITE,
            IntPtr.Zero,
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL,
            IntPtr.Zero);

        if (_handle == new IntPtr(-1))
        {
            throw new Exception("Failed to connect to ACPI device.");
        }
    }

    /// <summary>
    /// Sends a control code to a device driver for performing a specific operation.
    /// </summary>
    /// <param name="dwIoControlCode">The control code for the operation.</param>
    /// <param name="lpInBuffer">The input buffer.</param>
    /// <param name="lpOutBuffer">The output buffer.</param>
    private void Control(uint dwIoControlCode, byte[] lpInBuffer, byte[] lpOutBuffer)
    {
        uint lpBytesReturned = 0;
        bool result = DeviceIoControl(
            _handle,
            dwIoControlCode,
            lpInBuffer,
            (uint)lpInBuffer.Length,
            lpOutBuffer,
            (uint)lpOutBuffer.Length,
            ref lpBytesReturned,
            IntPtr.Zero);

        if (!result)
        {
            throw new InvalidOperationException("Device control operation failed.");
        }
    }

    /// <summary>
    /// Calls a specific method on the device.
    /// </summary>
    /// <param name="methodID">The method identifier.</param>
    /// <param name="args">The arguments for the method.</param>
    /// <returns>The result of the method call.</returns>
    private byte[] CallMethod(uint methodID, byte[] args)
    {
        byte[] acpiBuf = new byte[8 + args.Length];
        byte[] outBuffer = new byte[20];

        BitConverter.GetBytes(methodID).CopyTo(acpiBuf, 0);
        BitConverter.GetBytes(args.Length).CopyTo(acpiBuf, 4);
        Array.Copy(args, 0, acpiBuf, 8, args.Length);

        Control(ControlCode, acpiBuf, outBuffer);

        return outBuffer;
    }

    /// <summary>
    /// Sets the value for a specific device.
    /// </summary>
    /// <param name="deviceID">The device identifier.</param>
    /// <param name="status">The status to set.</param>
    public void DeviceSet(uint deviceID, int status)
    {
        byte[] args = new byte[8];
        BitConverter.GetBytes(deviceID).CopyTo(args, 0);
        BitConverter.GetBytes(status).CopyTo(args, 4);
        CallMethod(DEVS, args);
    }

    /// <summary>
    /// Gets the status of a specific device.
    /// </summary>
    /// <param name="deviceID">The device identifier.</param>
    /// <returns>The device status.</returns>
    public int DeviceGet(uint deviceID)
    {
        byte[] args = new byte[8];
        BitConverter.GetBytes(deviceID).CopyTo(args, 0);
        byte[] status = CallMethod(DSTS, args);
        return BitConverter.ToInt32(status, 0) - 65536;
    }

    /// <summary>
    /// Subscribes to WMI events.
    /// </summary>
    /// <param name="eventHandler">The event handler to invoke when an event occurs.</param>
    public static void SubscribeToEvents(Action<object, EventArrivedEventArgs> eventHandler)
    {
        ManagementEventWatcher watcher = new ManagementEventWatcher
        {
            Scope = new ManagementScope("root\\wmi"),
            Query = new WqlEventQuery("SELECT * FROM AsusAtkWmiEvent")
        };
        watcher.EventArrived += new EventArrivedEventHandler(eventHandler);
        watcher.Start();
    }

    /// <summary>
    /// Releases unmanaged resources used by the ASUSWmi class.
    /// </summary>
    public void Dispose()
    {
        if (_handle != IntPtr.Zero)
        {
            CloseHandle(_handle);
            _handle = IntPtr.Zero;
        }

        GC.SuppressFinalize(this);
    }
}