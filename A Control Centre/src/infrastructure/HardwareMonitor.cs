using System.Diagnostics;
using System.Security.Principal;
using A_Control_Centre.utilities;

namespace A_Control_Centre.infrastructure;

/// <summary>
/// Provides functionalities to monitor hardware sensor data such as CPU temperature, GPU temperature, and battery discharge rate.
/// </summary>
public class HardwareMonitor
{
    private static readonly GPUManager _gpuManager = new();

    internal static float? CPUTemp { get; private set; }
    internal static float? GPUTemp { get; private set; }
    internal static float? BatteryDischarge { get; private set; }

    public static string? CPUFan { get; private set; }
    public static string? GPUFan { get; private set; }

    /// <summary>
    /// Reads the sensor data for CPU temperature, battery discharge, and GPU temperature.
    /// </summary>
    /// <summary>
    /// Reads various sensor values from the system, including CPU and GPU temperatures, fan speeds, and battery discharge rate.
    /// </summary>
    public static void ReadSensors()
    {
        // Initialize sensor values to indicate that they're not yet read or are unavailable.
        BatteryDischarge = -1;
        GPUTemp = -1;

        // Attempt to read CPU temperature from WMI.
        CPUTemp = ReadCPUTemperature();

        // Attempt to read GPU temperature.
        ReadGPUTemperature();

        // Attempt to read battery discharge rate.
        ReadBatteryDischarge();
    }

    /// <summary>
    /// Attempts to read the CPU temperature from WMI or fallback to a Performance Counter.
    /// </summary>
    /// <returns>The CPU temperature, or -1 if unable to read.</returns>
    private static int ReadCPUTemperature()
    {
        int temperature = Program.WMI.DeviceGet(AsusWMI.TempCPU);
        if (temperature >= 0) return temperature;

        try
        {
            using var counter = new PerformanceCounter("Thermal Zone Information", "Temperature", @"\_TZ.THRM", true);
            return (int)(counter.NextValue() - 273); // Convert from Kelvin to Celsius
        }
        catch
        {
            Logger.WriteLine("Failed reading CPU temperature.");
            return -1;
        }
    }

    /// <summary>
    /// Attempts to read the GPU temperature, either from a custom provider or WMI as a fallback.
    /// </summary>
    private static void ReadGPUTemperature()
    {
        try
        {
            GPUTemp = _gpuManager.GetCurrentTemperature() ?? -1;

            if (GPUTemp < 0) // Fallback to WMI if the custom provider fails or returns an invalid value.
            {
                GPUTemp = Program.WMI.DeviceGet(AsusWMI.TempGPU);
            }
        }
        catch (Exception ex)
        {
            GPUTemp = -1;
            Logger.WriteLine("Failed reading GPU temperature: " + ex);
        }
    }

    /// <summary>
    /// Attempts to read the battery discharge rate using a Performance Counter.
    /// </summary>
    private static void ReadBatteryDischarge()
    {
        try
        {
            using var counter = new PerformanceCounter("Power Meter", "Power", "Power Meter (0)", true);
            BatteryDischarge = counter.NextValue() / 1000; // Convert to a more manageable unit if necessary.
        }
        catch
        {
            Logger.WriteLine("Failed reading battery discharge rate.");
        }
    }
}