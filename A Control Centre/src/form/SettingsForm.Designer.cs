﻿namespace A_Control_Centre.form;

partial class SettingsForm
{
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }

        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    ///  Required method for Designer support - do not modify
    ///  the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        buttonTurbo = new Button();
        buttonUltimate = new Button();
        buttonStandard = new Button();
        buttonEco = new Button();
        buttonBalanced = new Button();
        buttonSilent = new Button();
        labelBattery = new Label();
        buttonKeyboardColor = new Button();
        comboKeyboard = new ComboBox();
        label1 = new Label();
        checkBoost = new CheckBox();
        checkScreen = new CheckBox();
        labelScreen = new Label();
        buttonQuit = new Button();
        checkGPU = new CheckBox();
        labelPerf = new Label();
        labelCPUFan = new Label();
        tablePerf = new TableLayoutPanel();
        labelGPU = new Label();
        labelGPUFan = new Label();
        tableGPU = new TableLayoutPanel();
        labelBatteryTitle = new Label();
        trackBattery = new TrackBar();
        checkStartup = new CheckBox();
        tableLayoutPanel1 = new TableLayoutPanel();
        button144Hz = new Button();
        button60Hz = new Button();
        tablePerf.SuspendLayout();
        tableGPU.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)trackBattery).BeginInit();
        tableLayoutPanel1.SuspendLayout();
        SuspendLayout();
        // 
        // buttonTurbo
        // 
        buttonTurbo.BackColor = SystemColors.ControlLightLight;
        buttonTurbo.Dock = DockStyle.Fill;
        buttonTurbo.FlatAppearance.BorderColor = Color.Red;
        buttonTurbo.FlatAppearance.BorderSize = 0;
        buttonTurbo.FlatStyle = FlatStyle.Flat;
        buttonTurbo.ForeColor = SystemColors.ControlText;
        buttonTurbo.Location = new Point(344, 9);
        buttonTurbo.Margin = new Padding(6, 9, 6, 9);
        buttonTurbo.Name = "buttonTurbo";
        buttonTurbo.Size = new Size(157, 63);
        buttonTurbo.TabIndex = 2;
        buttonTurbo.Text = "Turbo";
        buttonTurbo.UseVisualStyleBackColor = false;
        buttonTurbo.Click += ButtonTurbo_Click;
        // 
        // buttonUltimate
        // 
        buttonUltimate.BackColor = SystemColors.ControlLightLight;
        buttonUltimate.Dock = DockStyle.Fill;
        buttonTurbo.FlatAppearance.BorderColor = Color.Red;
        buttonUltimate.FlatAppearance.BorderSize = 0;
        buttonUltimate.FlatStyle = FlatStyle.Flat;
        buttonUltimate.ForeColor = SystemColors.ControlText;
        buttonUltimate.Location = new Point(344, 9);
        buttonUltimate.Margin = new Padding(6, 9, 6, 9);
        buttonUltimate.Name = "buttonUltimate";
        buttonUltimate.Size = new Size(157, 63);
        buttonUltimate.TabIndex = 2;
        buttonUltimate.Text = "Ultimate";
        buttonUltimate.UseVisualStyleBackColor = false;
        buttonUltimate.Click += ButtonUltimate_Click;
        // 
        // buttonStandard
        // 
        buttonStandard.BackColor = SystemColors.ControlLightLight;
        buttonStandard.Dock = DockStyle.Fill;
        buttonStandard.FlatAppearance.BorderSize = 0;
        buttonBalanced.FlatAppearance.BorderColor = Color.Yellow;
        buttonStandard.FlatStyle = FlatStyle.Flat;
        buttonStandard.ForeColor = SystemColors.ControlText;
        buttonStandard.Location = new Point(175, 9);
        buttonStandard.Margin = new Padding(6, 9, 6, 9);
        buttonStandard.Name = "buttonStandard";
        buttonStandard.Size = new Size(157, 63);
        buttonStandard.TabIndex = 1;
        buttonStandard.Text = "Standard";
        buttonStandard.UseVisualStyleBackColor = false;
        buttonStandard.Click += ButtonStandard_Click;
        // 
        // buttonEco
        // 
        buttonEco.BackColor = SystemColors.ControlLightLight;
        buttonEco.CausesValidation = false;
        buttonSilent.FlatAppearance.BorderColor = Color.Lime;
        buttonEco.Dock = DockStyle.Fill;
        buttonEco.FlatAppearance.BorderSize = 0;
        buttonEco.FlatStyle = FlatStyle.Flat;
        buttonEco.ForeColor = SystemColors.ControlText;
        buttonEco.Location = new Point(6, 9);
        buttonEco.Margin = new Padding(6, 9, 6, 9);
        buttonEco.Name = "buttonEco";
        buttonEco.Size = new Size(157, 63);
        buttonEco.TabIndex = 0;
        buttonEco.Text = "Eco";
        buttonEco.UseVisualStyleBackColor = false;
        buttonEco.Click += ButtonEco_Click;
        // 
        // buttonBalanced
        // 
        buttonBalanced.BackColor = SystemColors.ControlLightLight;
        buttonBalanced.Dock = DockStyle.Fill;
        buttonBalanced.FlatAppearance.BorderColor = Color.Yellow;
        buttonBalanced.FlatAppearance.BorderSize = 0;
        buttonBalanced.FlatStyle = FlatStyle.Flat;
        buttonBalanced.ForeColor = SystemColors.ControlText;
        buttonBalanced.Location = new Point(175, 9);
        buttonBalanced.Margin = new Padding(6, 9, 6, 9);
        buttonBalanced.Name = "buttonBalanced";
        buttonBalanced.Size = new Size(157, 63);
        buttonBalanced.TabIndex = 1;
        buttonBalanced.Text = "Balanced";
        buttonBalanced.UseVisualStyleBackColor = false;
        buttonBalanced.Click += ButtonBalanced_Click;
        // 
        // buttonSilent
        // 
        buttonSilent.BackColor = SystemColors.ControlLightLight;
        buttonSilent.CausesValidation = false;
        buttonSilent.Dock = DockStyle.Fill;
        buttonSilent.FlatAppearance.BorderColor = Color.Lime;
        buttonSilent.FlatAppearance.BorderSize = 0;
        buttonSilent.FlatStyle = FlatStyle.Flat;
        buttonSilent.ForeColor = SystemColors.ControlText;
        buttonSilent.Location = new Point(6, 9);
        buttonSilent.Margin = new Padding(6, 9, 6, 9);
        buttonSilent.Name = "buttonSilent";
        buttonSilent.Size = new Size(157, 63);
        buttonSilent.TabIndex = 0;
        buttonSilent.Text = "Silent";
        buttonSilent.UseVisualStyleBackColor = false;
        buttonSilent.Click += ButtonSilent_Click;
        // 
        // labelBattery
        // 
        labelBattery.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        labelBattery.Location = new Point(313, 649);
        labelBattery.Name = "labelBattery";
        labelBattery.Size = new Size(207, 24);
        labelBattery.TabIndex = 52;
        labelBattery.Text = "                ";
        labelBattery.TextAlign = ContentAlignment.TopRight;
        // 
        // buttonKeyboardColor
        // 
        buttonKeyboardColor.AutoSize = true;
        buttonKeyboardColor.BackColor = SystemColors.ControlLightLight;
        buttonKeyboardColor.FlatAppearance.BorderColor = Color.Red;
        buttonKeyboardColor.FlatAppearance.BorderSize = 2;
        buttonKeyboardColor.FlatStyle = FlatStyle.Flat;
        buttonKeyboardColor.ForeColor = SystemColors.ControlText;
        buttonKeyboardColor.Location = new Point(197, 580);
        buttonKeyboardColor.Margin = new Padding(0);
        buttonKeyboardColor.Name = "buttonKeyboardColor";
        buttonKeyboardColor.Size = new Size(159, 44);
        buttonKeyboardColor.TabIndex = 51;
        buttonKeyboardColor.Text = "Color";
        buttonKeyboardColor.UseVisualStyleBackColor = false;
        buttonKeyboardColor.Click += ButtonKeyboardColor_Click;
        // 
        // comboKeyboard
        // 
        comboKeyboard.BackColor = SystemColors.Window;
        comboKeyboard.DropDownStyle = ComboBoxStyle.DropDownList;
        comboKeyboard.FlatStyle = FlatStyle.Flat;
        comboKeyboard.Font = new Font("Segoe UI", 10F);
        comboKeyboard.ForeColor = SystemColors.WindowText;
        comboKeyboard.FormattingEnabled = true;
        comboKeyboard.ItemHeight = 28;
        comboKeyboard.Items.AddRange(new object[] { "Static", "Breathe", "Strobe", "Rainbow" });
        comboKeyboard.Location = new Point(32, 580);
        comboKeyboard.Margin = new Padding(0);
        comboKeyboard.Name = "comboKeyboard";
        comboKeyboard.Size = new Size(151, 36);
        comboKeyboard.TabIndex = 50;
        comboKeyboard.TabStop = false;
        comboKeyboard.SelectedValueChanged += ComboKeyboard_SelectedValueChanged;
        // 
        // label1
        // 
        label1.AutoSize = true;
        label1.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
        label1.Location = new Point(29, 543);
        label1.Name = "label1";
        label1.Size = new Size(158, 25);
        label1.TabIndex = 48;
        label1.Text = "Laptop Keyboard";
        // 
        // checkBoost
        // 
        checkBoost.AutoSize = true;
        checkBoost.ForeColor = SystemColors.ControlText;
        checkBoost.Location = new Point(29, 136);
        checkBoost.Margin = new Padding(3, 2, 3, 2);
        checkBoost.Name = "checkBoost";
        checkBoost.Size = new Size(231, 29);
        checkBoost.TabIndex = 47;
        checkBoost.Text = "Enable CPU Turbo Boost";
        checkBoost.UseVisualStyleBackColor = true;
        checkBoost.Click += CheckBoost_Click;
        // 
        // checkScreen
        // 
        checkScreen.AutoSize = true;
        checkScreen.ForeColor = SystemColors.ControlText;
        checkScreen.Location = new Point(29, 481);
        checkScreen.Margin = new Padding(3, 2, 3, 2);
        checkScreen.Name = "checkScreen";
        checkScreen.Size = new Size(327, 29);
        checkScreen.TabIndex = 46;
        checkScreen.Text = "Lower display refresh rate on battery";
        checkScreen.UseVisualStyleBackColor = true;
        checkScreen.Click += checkScreen_CheckedChanged;
        // 
        // labelScreen
        // 
        labelScreen.AutoSize = true;
        labelScreen.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
        labelScreen.Location = new Point(27, 358);
        labelScreen.Name = "labelScreen";
        labelScreen.Size = new Size(185, 25);
        labelScreen.TabIndex = 43;
        labelScreen.Text = "Laptop Refresh Rate";
        // 
        // buttonQuit
        // 
        buttonQuit.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        buttonQuit.BackColor = SystemColors.ButtonFace;
        buttonQuit.Location = new Point(437, 754);
        buttonQuit.Margin = new Padding(3, 2, 3, 2);
        buttonQuit.Name = "buttonQuit";
        buttonQuit.Size = new Size(90, 36);
        buttonQuit.TabIndex = 42;
        buttonQuit.Text = "Quit";
        buttonQuit.UseVisualStyleBackColor = false;
        buttonQuit.Click += ButtonQuit_Click;
        // 
        // checkGPU
        // 
        checkGPU.AutoSize = true;
        checkGPU.ForeColor = SystemColors.ControlText;
        checkGPU.Location = new Point(29, 307);
        checkGPU.Margin = new Padding(3, 2, 3, 2);
        checkGPU.Name = "checkGPU";
        checkGPU.Size = new Size(193, 29);
        checkGPU.TabIndex = 41;
        checkGPU.Text = "Turn Eco on battery";
        checkGPU.UseVisualStyleBackColor = true;
        checkGPU.Click += CheckGPU_CheckedChanged;
        // 
        // labelPerf
        // 
        labelPerf.AutoSize = true;
        labelPerf.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
        labelPerf.Location = new Point(27, 26);
        labelPerf.Name = "labelPerf";
        labelPerf.Size = new Size(95, 25);
        labelPerf.TabIndex = 39;
        labelPerf.Text = "Fan Mode";
        // 
        // labelCPUFan
        // 
        labelCPUFan.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        labelCPUFan.Location = new Point(313, 26);
        labelCPUFan.Name = "labelCPUFan";
        labelCPUFan.Size = new Size(207, 24);
        labelCPUFan.TabIndex = 38;
        labelCPUFan.Text = "CPU Fan : 0%";
        labelCPUFan.TextAlign = ContentAlignment.TopRight;
        // 
        // tablePerf
        // 
        tablePerf.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        tablePerf.ColumnCount = 3;
        tablePerf.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.33333F));
        tablePerf.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.33333F));
        tablePerf.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.33333F));
        tablePerf.Controls.Add(buttonTurbo, 2, 0);
        tablePerf.Controls.Add(buttonBalanced, 1, 0);
        tablePerf.Controls.Add(buttonSilent, 0, 0);
        tablePerf.Location = new Point(21, 55);
        tablePerf.Margin = new Padding(3, 2, 3, 2);
        tablePerf.Name = "tablePerf";
        tablePerf.RowCount = 1;
        tablePerf.RowStyles.Add(new RowStyle(SizeType.Absolute, 81F));
        tablePerf.Size = new Size(507, 81);
        tablePerf.TabIndex = 37;
        // 
        // labelGPU
        // 
        labelGPU.AutoSize = true;
        labelGPU.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
        labelGPU.Location = new Point(27, 194);
        labelGPU.Name = "labelGPU";
        labelGPU.Size = new Size(103, 25);
        labelGPU.TabIndex = 35;
        labelGPU.Text = "GPU Mode";
        // 
        // labelGPUFan
        // 
        labelGPUFan.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        labelGPUFan.Location = new Point(313, 194);
        labelGPUFan.Name = "labelGPUFan";
        labelGPUFan.Size = new Size(207, 24);
        labelGPUFan.TabIndex = 34;
        labelGPUFan.Text = "GPU Fan : 0%";
        labelGPUFan.TextAlign = ContentAlignment.TopRight;
        // 
        // tableGPU
        // 
        tableGPU.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        tableGPU.ColumnCount = 3;
        tableGPU.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.33333F));
        tableGPU.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.33333F));
        tableGPU.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.33333F));
        tableGPU.Controls.Add(buttonUltimate, 2, 0);
        tableGPU.Controls.Add(buttonStandard, 1, 0);
        tableGPU.Controls.Add(buttonEco, 0, 0);
        tableGPU.Location = new Point(21, 226);
        tableGPU.Margin = new Padding(3, 2, 3, 2);
        tableGPU.Name = "tableGPU";
        tableGPU.RowCount = 1;
        tableGPU.RowStyles.Add(new RowStyle(SizeType.Absolute, 81F));
        tableGPU.Size = new Size(507, 81);
        tableGPU.TabIndex = 33;
        // 
        // labelBatteryTitle
        // 
        labelBatteryTitle.AutoSize = true;
        labelBatteryTitle.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
        labelBatteryTitle.Location = new Point(29, 648);
        labelBatteryTitle.Name = "labelBatteryTitle";
        labelBatteryTitle.Size = new Size(188, 25);
        labelBatteryTitle.TabIndex = 31;
        labelBatteryTitle.Text = "Battery Charge Limit";
        // 
        // trackBattery
        // 
        trackBattery.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        trackBattery.LargeChange = 20;
        trackBattery.Location = new Point(20, 679);
        trackBattery.Margin = new Padding(3, 2, 3, 2);
        trackBattery.Maximum = 100;
        trackBattery.Minimum = 50;
        trackBattery.Name = "trackBattery";
        trackBattery.Size = new Size(507, 69);
        trackBattery.SmallChange = 10;
        trackBattery.TabIndex = 30;
        trackBattery.TickFrequency = 10;
        trackBattery.TickStyle = TickStyle.TopLeft;
        trackBattery.Value = 100;
        trackBattery.Scroll += TrackBatteryChange;
        // 
        // checkStartup
        // 
        checkStartup.AutoSize = true;
        checkStartup.Location = new Point(35, 760);
        checkStartup.Margin = new Padding(3, 2, 3, 2);
        checkStartup.Name = "checkStartup";
        checkStartup.Size = new Size(157, 29);
        checkStartup.TabIndex = 29;
        checkStartup.Text = "Run on Startup";
        checkStartup.UseVisualStyleBackColor = true;
        checkStartup.Click += CheckStartup_CheckedChanged;
        // 
        // tableLayoutPanel1
        // 
        tableLayoutPanel1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        tableLayoutPanel1.ColumnCount = 3;
        tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.33333F));
        tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.33333F));
        tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.33333F));
        tableLayoutPanel1.Controls.Add(button144Hz, 1, 0);
        tableLayoutPanel1.Controls.Add(button60Hz, 0, 0);
        tableLayoutPanel1.Location = new Point(21, 396);
        tableLayoutPanel1.Margin = new Padding(3, 2, 3, 2);
        tableLayoutPanel1.Name = "tableLayoutPanel1";
        tableLayoutPanel1.RowCount = 1;
        tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 81F));
        tableLayoutPanel1.Size = new Size(507, 81);
        tableLayoutPanel1.TabIndex = 53;
        // 
        // button144Hz
        // 
        button144Hz.BackColor = SystemColors.ControlLightLight;
        button144Hz.Dock = DockStyle.Fill;
        button144Hz.FlatAppearance.BorderSize = 0;
        button144Hz.FlatStyle = FlatStyle.Flat;
        button144Hz.ForeColor = SystemColors.ControlText;
        button144Hz.Location = new Point(175, 9);
        button144Hz.Margin = new Padding(6, 9, 6, 9);
        button144Hz.Name = "button144Hz";
        button144Hz.Size = new Size(157, 63);
        button144Hz.TabIndex = 1;
        button144Hz.Text = "144Hz + OD";
        button144Hz.UseVisualStyleBackColor = false;
        button144Hz.Click += Button144Hz_Click;
        // 
        // button60Hz
        // 
        button60Hz.BackColor = SystemColors.ControlLightLight;
        button60Hz.CausesValidation = false;
        button60Hz.Dock = DockStyle.Fill;
        button60Hz.FlatAppearance.BorderSize = 0;
        button60Hz.FlatStyle = FlatStyle.Flat;
        button60Hz.ForeColor = SystemColors.ControlText;
        button60Hz.Location = new Point(6, 9);
        button60Hz.Margin = new Padding(6, 9, 6, 9);
        button60Hz.Name = "button60Hz";
        button60Hz.Size = new Size(157, 63);
        button60Hz.TabIndex = 0;
        button60Hz.Text = "60Hz";
        button60Hz.UseVisualStyleBackColor = false;
        button60Hz.Click += Button60Hz_Click;
        // 
        // SettingsForm
        // 
        AutoScaleDimensions = new SizeF(144F, 144F);
        AutoScaleMode = AutoScaleMode.Dpi;
        BackColor = SystemColors.Control;
        ClientSize = new Size(548, 814);
        Controls.Add(tableLayoutPanel1);
        Controls.Add(labelBattery);
        Controls.Add(buttonKeyboardColor);
        Controls.Add(comboKeyboard);
        Controls.Add(label1);
        Controls.Add(checkBoost);
        Controls.Add(checkScreen);
        Controls.Add(labelScreen);
        Controls.Add(buttonQuit);
        Controls.Add(checkGPU);
        Controls.Add(labelPerf);
        Controls.Add(labelCPUFan);
        Controls.Add(tablePerf);
        Controls.Add(labelGPU);
        Controls.Add(labelGPUFan);
        Controls.Add(tableGPU);
        Controls.Add(labelBatteryTitle);
        Controls.Add(trackBattery);
        Controls.Add(checkStartup);
        FormBorderStyle = FormBorderStyle.FixedSingle;
        MaximizeBox = false;
        MdiChildrenMinimizedAnchorBottom = false;
        MinimizeBox = false;
        Name = "SettingsForm";
        Padding = new Padding(6, 9, 6, 9);
        ShowIcon = false;
        StartPosition = FormStartPosition.CenterScreen;
        Text = "A Control Centre";
        FormClosing += SettingsForm_FormClosing;
        Load += Settings_Load;
        VisibleChanged += SettingsForm_VisibleChanged;
        tablePerf.ResumeLayout(false);
        tableGPU.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)trackBattery).EndInit();
        tableLayoutPanel1.ResumeLayout(false);
        ResumeLayout(false);
        PerformLayout();
    }

    #endregion

    private Button buttonTurbo;
    private Button buttonUltimate;
    private Button buttonStandard;
    private Button buttonEco;
    private Button buttonBalanced;
    private Button buttonSilent;
    private Label labelBattery;
    private Button buttonKeyboardColor;
    private ComboBox comboKeyboard;
    private Label label1;
    private CheckBox checkBoost;
    private CheckBox checkScreen;
    private Label labelScreen;
    private Button buttonQuit;
    private CheckBox checkGPU;
    private Label labelPerf;
    private Label labelCPUFan;
    private TableLayoutPanel tablePerf;
    private Label labelGPU;
    private Label labelGPUFan;
    private TableLayoutPanel tableGPU;
    private Label labelBatteryTitle;
    private TrackBar trackBattery;
    private CheckBox checkStartup;
    private TableLayoutPanel tableLayoutPanel1;
    private Button button144Hz;
    private Button button60Hz;
}