using System.Diagnostics;
using System.Timers;
using A_Control_Centre.hardware;
using A_Control_Centre.infrastructure;
using A_Control_Centre.utilities;
using Timer = System.Timers.Timer;

namespace A_Control_Centre.form;

/// <summary>
/// Represents the settings form for adjusting performance and visual settings.
/// </summary>
public partial class SettingsForm : Form
{
    // Define constants for button state.
    private const int ButtonInactive = 0;
    private const int ButtonActive = 5;

    // Timer for handling repeated or scheduled tasks.
    private static Timer _aTimer;

    /// <summary>
    /// Gets or sets the name of the current performance mode.
    /// </summary>
    private string PerfName { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="SettingsForm"/> class.
    /// </summary>
    public SettingsForm()
    {
        InitializeComponent();

        // Set combo box 1st index
        comboKeyboard.SelectedIndex = 0;

        // Initialize the timer for scheduled tasks.
        SetTimer();
    }

    /// <summary>
    /// Handles the keyboard color button click event to open a color dialog and set the Backlight color.
    /// </summary>
    /// <param name="sender">The object that raised the event.</param>
    /// <param name="e">An EventArgs that contains the event data.</param>
    private void ButtonKeyboardColor_Click(object? sender, EventArgs e)
    {
        // Guard clause to ensure the sender is not null and is a Button.
        if (sender is not Button but)
            return;

        using ColorDialog colorDlg = new ColorDialog();
        // Disables the user from selecting a custom color.
        colorDlg.AllowFullOpen = false;
        // Sets the initial color select to the current border color of the button.
        colorDlg.Color = but.FlatAppearance.BorderColor;

        // Shows the color dialog and checks the user's action.
        if (colorDlg.ShowDialog() == DialogResult.OK)
        {
            // Sets the chosen color to Backlight and updates the configuration.
            SetBacklightColor(colorDlg.Color);
        }
    }

    /// <summary>
    /// Initializes the Backlight settings from the configuration.
    /// </summary>
    public void InitBacklight()
    {
        // Retrieves Backlight settings from the configuration.
        Backlight.Modes mode =
            (Backlight.Modes)Program.Config.GetConfig("backlight_mode", defaultValue: (int)Backlight.Modes.Static);
        int colorCode = Program.Config.GetConfig("backlight_color", defaultValue: Color.Lime.ToArgb());

        // the default color is white.
        Color color = Color.FromArgb(colorCode);

        // Applies the Backlight color and mode settings.
        SetBacklightColor(color, apply: false);
        SetBacklightMode((int)mode, apply: false);

        // Updates the Backlight mode.
        Backlight.Mode = mode;
    }

    /// <summary>
    /// Sets the Backlight color and updates the configuration and UI.
    /// </summary>
    /// <param name="color">The color to be set for the Backlight.</param>
    /// <param name="apply">Indicates whether to apply the color immediately.</param>
    private void SetBacklightColor(Color color, bool apply = true)
    {
        // Updates the Backlight color.
        Backlight.Color1 = color;
        // Saves the color setting.
        Program.Config.SetConfig("backlight_color", color.ToArgb());

        // Applies the Backlight settings if requested.
        if (apply)
            Backlight.ApplyBacklight();

        // Updates the button's border color to match the Backlight color.
        buttonKeyboardColor.FlatAppearance.BorderColor = color;
    }

    /// <summary>
    /// Sets the Backlight mode, updates the configuration and UI.
    /// </summary>
    /// <param name="mode">The Backlight mode to be set.</param>
    /// <param name="apply">Indicates whether to apply the mode immediately.</param>
    private void SetBacklightMode(int mode = 0, bool apply = true)
    {
        // Ensures mode is within a valid range.
        mode %= 4; // Assuming there are only 4 modes (0-3).

        // If the mode is unchanged, nothing to do.
        if (Backlight.Mode == (Backlight.Modes)mode) return;

        // Updates the Backlight mode.
        Backlight.Mode = (Backlight.Modes)mode;
        // Saves the mode setting.
        Program.Config.SetConfig("backlight_mode", mode);

        // Temporarily unsubscribes the event to prevent recursion.
        comboKeyboard.SelectedValueChanged -= ComboKeyboard_SelectedValueChanged;
        // Updates the selected index to reflect the new mode.
        comboKeyboard.SelectedIndex = mode;
        // Re-subscribes to the event after the update.
        comboKeyboard.SelectedValueChanged += ComboKeyboard_SelectedValueChanged;

        // Applies the Backlight settings if requested.
        if (apply)
            Backlight.ApplyBacklight();
    }

    /// <summary>
    /// Cycles through the Backlight modes.
    /// </summary>
    public void CycleBacklightMode()
    {
        // Increments the current Backlight mode and sets it.
        SetBacklightMode((Program.Config.GetConfig("backlight_mode") + 1) % 4);
    }

    /// <summary>
    /// Handles the keyboard combo box value changed event to change the Backlight mode.
    /// </summary>
    /// <param name="sender">The object that raised the event.</param>
    /// <param name="e">An EventArgs that contains the event data.</param>
    private void ComboKeyboard_SelectedValueChanged(object? sender, EventArgs e)
    {
        // Guard clause to ensure the sender is not null and is a ComboBox.
        if (sender is not ComboBox cmb)
            return;

        // Sets the Backlight mode to the selected index of the combo box.
        SetBacklightMode(cmb.SelectedIndex);
    }

    /// <summary>
    /// Handles the click event on the CPU Boost checkbox. Enables or disables CPU boost based on the checkbox state.
    /// </summary>
    /// <param name="sender">The checkbox that triggered the event.</param>
    /// <param name="e">The event arguments.</param>
    private void CheckBoost_Click(object? sender, EventArgs e)
    {
        if (sender is not CheckBox chk) return; // Use pattern matching to simplify null check and casting.
        // Determine the boost level based on the checkbox's checked state and set it accordingly.
        int boostLevel = chk.Checked ? 3 : 0;
        NativeMethods.SetCPUBoost(boostLevel);
    }

    /// <summary>
    /// Sets the screen refresh rate to a high frequency (e.g., 144Hz) when the corresponding button is clicked.
    /// </summary>
    /// <param name="sender">The button that triggered the event.</param>
    /// <param name="e">The event arguments.</param>
    private void Button144Hz_Click(object? sender, EventArgs e)
    {
        SetScreenRefreshRateAndOverdrive(1000, 1); // Using 1000 as a special value to indicate high frequency.
    }

    /// <summary>
    /// Sets the screen refresh rate to a lower frequency (e.g., 60Hz) when the corresponding button is clicked.
    /// </summary>
    /// <param name="sender">The button that triggered the event.</param>
    /// <param name="e">The event arguments.</param>
    private void Button60Hz_Click(object? sender, EventArgs e)
    {
        SetScreenRefreshRateAndOverdrive(60, 0);
    }

    /// <summary>
    /// Sets the screen refresh rate and overdrive mode based on the provided parameters.
    /// </summary>
    /// <param name="frequency">The desired screen refresh rate.</param>
    /// <param name="overdrive">The overdrive setting to apply.</param>
    private void SetScreenRefreshRateAndOverdrive(int frequency = -1, int overdrive = -1)
    {
        // Attempt to get the current screen refresh rate.
        int currentFrequency = NativeMethods.GetRefreshRate();

        // If the current refresh rate is not detected, initialize screen settings and exit.
        if (currentFrequency < 0)
        {
            InitScreen();
            return;
        }

        // Adjust frequency if a special value (1000 or more) is provided.
        if (frequency >= 1000)
        {
            frequency = Program.Config.GetConfig("max_frequency", 144);
            // Default to 144Hz if the configuration specifies 60Hz or less.
            if (frequency <= 60)
            {
                frequency = 144;
            }
        }

        // Exit if no valid frequency is specified.
        if (frequency <= 0) return;

        // Set the new refresh rate.
        NativeMethods.SetRefreshRate(frequency);

        // Apply overdrive setting if specified.
        if (overdrive > 0)
        {
            Program.WMI.DeviceSet(AsusWMI.ScreenOverdrive, overdrive);
        }

        // Reinitialize screen settings to apply changes.
        InitScreen();

        // Log the action.
        Logger.WriteLine($"Screen set to {frequency}Hz");
    }

    /// <summary>
    /// Initializes CPU boost settings based on the current system configuration.
    /// </summary>
    public void InitBoost()
    {
        int boost = NativeMethods.GetCPUBoost();
        checkBoost.Checked = boost > 0;
    }

    /// <summary>
    /// Initializes screen settings UI based on the current refresh rate and overdrive settings.
    /// </summary>
    private void InitScreen()
    {
        int frequency = NativeMethods.GetRefreshRate();
        int maxFrequency = Program.Config.GetConfig("max_frequency", 144); // Default to 144 if no config found.

        // Disable screen settings UI if frequency is invalid.
        if (frequency < 0)
        {
            ConfigureScreenSettingsUI(enabled: false, "Laptop Screen: Turned off");
        }
        else
        {
            ConfigureScreenSettingsUI(enabled: true, "Laptop Screen");
            UpdateScreenSettingsButtons(frequency, maxFrequency);
        }

        // Save current settings to configuration.
        Program.Config.SetConfig("frequency", frequency);
        Program.Config.SetConfig("overdrive", GetScreenOverdrive());
    }

    /// <summary>
    /// Configures the screen settings UI components based on the enabled state and screen status label.
    /// </summary>
    /// <param name="enabled">Whether the screen settings UI should be enabled.</param>
    /// <param name="statusLabel">The text to display on the screen status label.</param>
    private void ConfigureScreenSettingsUI(bool enabled, string statusLabel)
    {
        button60Hz.Enabled = button144Hz.Enabled = enabled;
        labelScreen.Text = statusLabel;

        Color backColor = enabled ? SystemColors.ControlLightLight : SystemColors.ControlLight;
        button60Hz.BackColor = button144Hz.BackColor = backColor;
    }

    /// <summary>
    /// Updates the button states and labels based on the current and maximum screen frequency.
    /// </summary>
    /// <param name="currentFrequency">The current screen frequency.</param>
    /// <param name="maxFrequency">The maximum screen frequency.</param>
    private void UpdateScreenSettingsButtons(int currentFrequency, int maxFrequency)
    {
        // Default button border styles.
        button60Hz.FlatAppearance.BorderSize = button144Hz.FlatAppearance.BorderSize = ButtonInactive;

        switch (currentFrequency)
        {
            case 60:
                button60Hz.FlatAppearance.BorderSize = ButtonActive; // Highlight the 60Hz button.
                break;
            case > 60:
                maxFrequency = currentFrequency; // Update max frequency if current is higher.
                Program.Config.SetConfig("max_frequency", maxFrequency);
                button144Hz.FlatAppearance.BorderSize = ButtonActive; // Highlight the 144Hz button.
                break;
        }

        // Update button text to include overdrive if applicable.
        if (maxFrequency > 60)
        {
            button144Hz.Text = $"{maxFrequency}Hz + OD";
        }
    }

    /// <summary>
    /// Retrieves the current screen overdrive setting, logging if not supported.
    /// </summary>
    /// <returns>The current screen overdrive setting.</returns>
    private static int GetScreenOverdrive()
    {
        try
        {
            return Program.WMI.DeviceGet(AsusWMI.ScreenOverdrive);
        }
        catch
        {
            Logger.WriteLine("Screen Overdrive not supported");
            return 0; // Return default value if overdrive is not supported.
        }
    }

    /// <summary>
    /// Handles the Quit button click event to close the application and hide the tray icon.
    /// </summary>
    /// <param name="sender">The object that raised the event.</param>
    /// <param name="e">Event data.</param>
    private void ButtonQuit_Click(object? sender, EventArgs e)
    {
        Program.trayIcon.Visible = false; // Ensure the tray icon is hidden when the application is closed.
        Application.Exit(); // Exit the application.
    }

    /// <summary>
    /// Handles the form's closing event to prevent the application from exiting when the user attempts to close the window.
    /// Instead, the form is hidden.
    /// </summary>
    /// <param name="sender">The object that raised the event.</param>
    /// <param name="e">Form closing event data, including the close reason.</param>
    private void SettingsForm_FormClosing(object? sender, FormClosingEventArgs e)
    {
        if (e.CloseReason != CloseReason.UserClosing) return;
        e.Cancel = true; // Prevent the form from closing.
        Hide(); // Hide the form instead.
    }

    /// <summary>
    /// Sets the GPU mode to Ultimate when the corresponding button is clicked.
    /// </summary>
    /// <param name="sender">The object that raised the event.</param>
    /// <param name="e">Event data.</param>
    private void ButtonUltimate_Click(object? sender, EventArgs e)
    {
        SetGPUMode(AsusWMI.GPUModes.Ultimate);
    }

    /// <summary>
    /// Sets the GPU mode to Standard when the corresponding button is clicked.
    /// </summary>
    /// <param name="sender">The object that raised the event.</param>
    /// <param name="e">Event data.</param>
    private void ButtonStandard_Click(object? sender, EventArgs e)
    {
        SetGPUMode(); //Standard is the default
    }

    /// <summary>
    /// Sets the GPU mode to Eco when the corresponding button is clicked.
    /// </summary>
    /// <param name="sender">The object that raised the event.</param>
    /// <param name="e">Event data.</param>
    private void ButtonEco_Click(object? sender, EventArgs e)
    {
        SetGPUMode(AsusWMI.GPUModes.Eco);
    }

    /// <summary>
    /// Configures a timer with a 500ms interval, which does not auto-reset and is initially disabled.
    /// </summary>
    private static void SetTimer()
    {
        _aTimer = new Timer(500); // Set the interval to 500 milliseconds.
        _aTimer.Elapsed += OnTimedEvent; // Subscribe to the Elapsed event.
        _aTimer.AutoReset = true; // Set the timer to auto-reset.
        _aTimer.Enabled = false; // Initially disable the timer.
    }

    /// <summary>
    /// Refreshes sensor readings (CPU/GPU temperatures, fan speeds, and battery status) and updates the UI accordingly.
    /// </summary>
    private static void RefreshSensors()
    {
        // Attempt to read sensor data. Catch and log any errors.
        try
        {
            HardwareMonitor.ReadSensors();
        }
        catch
        {
            Logger.WriteLine("Failed reading sensors");
        }

        // Format sensor readings for display.
        string cpuFan = $" Fan: {Math.Round(Program.WMI.DeviceGet(AsusWMI.CPUFan) / 0.6)}%";
        string gpuFan = $" Fan: {Math.Round(Program.WMI.DeviceGet(AsusWMI.GPUFan) / 0.6)}%";
        string cpuTemp = HardwareMonitor.CPUTemp > 0
            ? $": {Math.Round((decimal)HardwareMonitor.CPUTemp)}°C - "
            : "";
        string gpuTemp = HardwareMonitor.GPUTemp > 0
            ? $": {Math.Round((decimal)HardwareMonitor.GPUTemp)}°C - "
            : "";
        string battery = HardwareMonitor.BatteryDischarge > 0
            ? $"Discharging: {Math.Round((decimal)HardwareMonitor.BatteryDischarge, 1)}W"
            : "";

        // Update UI elements with the sensor readings.
        Program.SettingsForm.BeginInvoke(delegate
        {
            Program.SettingsForm.labelCPUFan.Text = "CPU" + cpuTemp + cpuFan;
            Program.SettingsForm.labelGPUFan.Text = "GPU" + gpuTemp + gpuFan;
            Program.SettingsForm.labelBattery.Text = battery;
        });
    }

    /// <summary>
    /// Handles the Elapsed event of a timer.
    /// </summary>
    /// <param name="source">The source of the event.</param>
    /// <param name="e">An <see cref="ElapsedEventArgs"/> that contains the event data.</param>
    private static void OnTimedEvent(object? source, ElapsedEventArgs? e)
    {
        RefreshSensors();
        _aTimer.Interval = 2000; // Sets the timer interval to 2000 milliseconds (2 seconds).
    }

    /// <summary>
    /// Handles the VisibleChanged event of the SettingsForm.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">An <see cref="EventArgs"/> that contains the event data.</param>
    private void SettingsForm_VisibleChanged(object? sender, EventArgs e)
    {
        if (Visible)
        {
            InitScreen(); // Initializes the screen settings.

            // Positions the form at the bottom right corner of the screen.
            Left = Screen.FromControl(this).Bounds.Width - 10 - Width;
            Top = Screen.FromControl(this).WorkingArea.Height - 10 - Height;
            Activate(); // Brings the form to the foreground.

            _aTimer.Interval = 500; // Sets the timer interval to 500 milliseconds (0.5 seconds).
            _aTimer.Enabled = true; // Starts the timer.
        }
        else
        {
            _aTimer.Enabled = false; // Stops the timer.
        }
    }

    /// <summary>
    /// Sets the performance mode of the system.
    /// </summary>
    /// <param name="performanceMode">The desired performance mode.</param>
    /// <param name="notify">Indicates whether to notify the user about the change.</param>
    public void SetPerformanceMode(AsusWMI.FanModes performanceMode = AsusWMI.FanModes.Balanced, bool notify = false)
    {
        // Resets the border style of all buttons to inactive.
        buttonSilent.FlatAppearance.BorderSize = ButtonInactive;
        buttonBalanced.FlatAppearance.BorderSize = ButtonInactive;
        buttonTurbo.FlatAppearance.BorderSize = ButtonInactive;

        // Sets the border style of the selected performance mode button to active and updates the performance mode name.
        switch (performanceMode)
        {
            case AsusWMI.FanModes.Silent:
                buttonSilent.FlatAppearance.BorderSize = ButtonActive;
                PerfName = "Silent";
                break;
            case AsusWMI.FanModes.Turbo:
                buttonTurbo.FlatAppearance.BorderSize = ButtonActive;
                PerfName = "Turbo";
                break;
            case AsusWMI.FanModes.Balanced:
            default:
                buttonBalanced.FlatAppearance.BorderSize = ButtonActive;
                PerfName = "Balanced";
                break;
        }

        // Saves the selected performance mode in the configuration.
        Program.Config.SetConfig("performance_mode", (int)performanceMode);
        try
        {
            // Attempts to set the selected performance mode.
            Program.WMI.DeviceSet(AsusWMI.PerformanceMode, (int)performanceMode);
        }
        catch
        {
            // If setting the performance mode is not supported, notify the user.
            labelPerf.Text = $"Performance Mode {performanceMode.ToString()} not supported!";
        }

        if (notify)
        {
            //TODO
        }
    }

    /// <summary>
    /// Cycles through the available performance modes.
    /// </summary>
    public void CyclePerformanceMode()
    {
        // Increments the current performance mode.
        int mode = (Program.Config.GetConfig("performance_mode") + 1) % 3;
        SetPerformanceMode((AsusWMI.FanModes)mode, true);
    }

    /// <summary>
    /// Automatically adjusts the screen brightness based on the power state.
    /// </summary>
    /// <param name="plugged">Indicates whether the device is plugged in (1) or not (0).</param>
    public void AutoScreen(bool plugged = true)
    {
        int screenAuto = Program.Config.GetConfig("screen_auto");
        if (screenAuto != 1) return;

        if (plugged)
            SetScreenRefreshRateAndOverdrive(1000, 1); // Sets high brightness for plugged-in state.
        else
            SetScreenRefreshRateAndOverdrive(60, 0); // Sets lower brightness for battery mode.

        InitScreen(); // Initializes the screen settings.
    }

    public void AutoGPUMode(bool plugged = true)
    {
        int gpuAuto = Program.Config.GetConfig("gpu_auto");
        if (gpuAuto != 1) return;

        int eco = Program.WMI.DeviceGet(AsusWMI.GPUEco);
        int mux = Program.WMI.DeviceGet(AsusWMI.GPUMux);

        if (mux == 0) // GPU in Ultimate, ignore
            return;

        switch (eco)
        {
            // Eco to Standard when plugged in
            case 1 when plugged:
                SetEcoMode(0);
                break;
            // Standard to Eco when unplugged
            case 0 when !plugged:
                SetEcoMode(1);
                break;
            default:
                AutoScreen(plugged);
                break;
        }
    }

    public AsusWMI.GPUModes InitGPUMode()
    {
        int eco = Program.WMI.DeviceGet(AsusWMI.GPUEco);
        int mux = Program.WMI.DeviceGet(AsusWMI.GPUMux);

        AsusWMI.GPUModes gpuMode;

        if (mux == 0)
            gpuMode = AsusWMI.GPUModes.Ultimate;
        else
        {
            gpuMode = eco == 1 ? AsusWMI.GPUModes.Eco : AsusWMI.GPUModes.Standard;

            buttonUltimate.Visible = (mux == 1);
        }

        ButtonEnabled(buttonEco, true);
        ButtonEnabled(buttonStandard, true);
        ButtonEnabled(buttonUltimate, true);

        Program.Config.SetConfig("gpu_mode", (int)gpuMode);
        VisualiseGPUMode(gpuMode);

        return gpuMode;
    }


    private void SetEcoMode(int eco)
    {
        ButtonEnabled(buttonEco, false);
        ButtonEnabled(buttonStandard, false);
        ButtonEnabled(buttonUltimate, false);

        labelGPU.Text = "GPU Mode: Changing ...";

        new Thread(() =>
        {
            Thread.CurrentThread.IsBackground = true;
            
            //Kills all processes using dGPU if iGPU is forced
            if (eco == 1)
            {
                foreach (var process in Process.GetProcessesByName("EADesktop")) process.Kill();
            }
            
            Program.WMI.DeviceSet(AsusWMI.GPUEco, eco);
            Program.SettingsForm.BeginInvoke(delegate
            {
                InitGPUMode();
                Thread.Sleep(500);
                AutoScreen(SystemInformation.PowerStatus.PowerLineStatus == PowerLineStatus.Online);
            });
        }).Start();
    }

    private static void ButtonEnabled(Button but, bool enabled)
    {
        but.Enabled = enabled;
        but.BackColor = enabled ? SystemColors.ControlLightLight : SystemColors.ControlLight;
    }

    private void SetGPUMode(AsusWMI.GPUModes gpuMode = AsusWMI.GPUModes.Standard)
    {
        AsusWMI.GPUModes currentGPU = (AsusWMI.GPUModes)Program.Config.GetConfig("gpu_mode");

        if (currentGPU.Equals(gpuMode))
            return;

        var restart = false;
        var changed = false;

        if (currentGPU.Equals(AsusWMI.GPUModes.Ultimate))
        {
            DialogResult dialogResult = MessageBox.Show("Switching off Ultimate Mode requires restart", "Reboot now?",
                MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Program.WMI.DeviceSet(AsusWMI.GPUMux, 1);
                restart = true;
                changed = true;
            }
        }
        else switch (gpuMode)
        {
            case AsusWMI.GPUModes.Ultimate:
            {
                DialogResult dialogResult =
                    MessageBox.Show(" Ultimate Mode requires restart", "Reboot now?", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    Program.WMI.DeviceSet(AsusWMI.GPUMux, 0);
                    restart = true;
                    changed = true;
                }

                break;
            }
            case AsusWMI.GPUModes.Eco:
                VisualiseGPUMode(gpuMode);
                SetEcoMode(1);
                changed = true;
                break;
            case AsusWMI.GPUModes.Standard:
            default:
                VisualiseGPUMode(gpuMode);
                SetEcoMode(0);
                changed = true;
                break;
        }

        if (changed)
        {
            Program.Config.SetConfig("gpu_mode", (int)gpuMode);
        }

        if (!restart) return;
        VisualiseGPUMode(gpuMode);
        Process.Start("shutdown", "/r /t 1");
    }

    /// <summary>
    /// Updates the UI to reflect the current auto GPU mode status.
    /// </summary>
    /// <param name="gpuAuto">The auto GPU mode status (1 for enabled, otherwise disabled).</param>
    public void VisualiseGPUAuto(int gpuAuto)
    {
        checkGPU.Checked = (gpuAuto == 1);
    }

    /// <summary>
    /// Updates the UI to reflect the current auto screen mode status.
    /// </summary>
    /// <param name="screenAuto">The auto screen mode status (1 for enabled, otherwise disabled).</param>
    public void VisualiseScreenAuto(int screenAuto)
    {
        checkScreen.Checked = (screenAuto == 1);
    }

    /// <summary>
    /// Visualizes the current GPU mode on the UI.
    /// </summary>
    /// <param name="gpuMode">The current GPU mode to visualize.</param>
    internal void VisualiseGPUMode(AsusWMI.GPUModes gpuMode)
    {
        // Reset all buttons to inactive state.
        buttonEco.FlatAppearance.BorderSize = ButtonInactive;
        buttonStandard.FlatAppearance.BorderSize = ButtonInactive;
        buttonUltimate.FlatAppearance.BorderSize = ButtonInactive;

        // Set the corresponding button and label based on the GPU mode.
        switch (gpuMode)
        {
            case AsusWMI.GPUModes.Eco:
                buttonEco.FlatAppearance.BorderSize = ButtonActive;
                buttonEco.FlatAppearance.BorderColor = Color.Lime;
                labelGPU.Text = "GPU Mode: iGPU only";
                break;
            case AsusWMI.GPUModes.Ultimate:
                buttonUltimate.FlatAppearance.BorderSize = ButtonActive;
                buttonUltimate.FlatAppearance.BorderColor = Color.Red;
                labelGPU.Text = "GPU Mode: dGPU only";
                break;
            case AsusWMI.GPUModes.Standard:
            default: // Assumes Standard mode as the default case.
                buttonStandard.FlatAppearance.BorderSize = ButtonActive;
                buttonStandard.FlatAppearance.BorderColor = Color.Yellow;
                labelGPU.Text = "GPU Mode: iGPU and dGPU";
                break;
        }
    }

    /// <summary>
    /// Sets the system's performance mode to Silent when the Silent button is clicked.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The event data.</param>
    private void ButtonSilent_Click(object? sender, EventArgs e)
    {
        SetPerformanceMode(AsusWMI.FanModes.Silent);
    }

    /// <summary>
    /// Sets the system's performance mode to Balanced when the Balanced button is clicked.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The event data.</param>
    private void ButtonBalanced_Click(object? sender, EventArgs e)
    {
        SetPerformanceMode(); //Balanced is the default
    }

    /// <summary>
    /// Sets the system's performance mode to Turbo when the Turbo button is clicked.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The event data.</param>
    private void ButtonTurbo_Click(object? sender, EventArgs e)
    {
        SetPerformanceMode(AsusWMI.FanModes.Turbo);
    }

    /// <summary>
    /// Handles the Load event of the Settings form.
    /// This method is currently a placeholder for any initialization code required when the form loads.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The event data.</param>
    private void Settings_Load(object sender, EventArgs e)
    {
        // Placeholder for form load initialization code.
    }

    /// <summary>
    /// Updates the startup check status based on the provided value.
    /// </summary>
    /// <param name="status">The status to set for the startup check, where true means checked.</param>
    public void SetStartupCheck(bool status)
    {
        checkStartup.Checked = status;
    }

    /// <summary>
    /// Handles the CheckedChanged event for the startup checkbox.
    /// Schedules or unschedules the startup task based on the checkbox's state.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The event data.</param>
    private void CheckStartup_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chk = (CheckBox)sender;
        if (chk.Checked)
        {
            Startup.Schedule();
        }
        else
        {
            Startup.UnSchedule();
        }
    }

    /// <summary>
    /// Sets the battery charge limit and updates the UI to reflect this change.
    /// Validates the limit to ensure it's within the acceptable range (50% to 100%).
    /// </summary>
    /// <param name="limit">The battery charge limit to set, defaulting to 100% if the provided value is outside the acceptable range.</param>
    public void SetBatteryChargeLimit(int limit = 100)
    {
        // Ensure the limit is within the acceptable range.
        limit = Math.Clamp(limit, 50, 100);

        // Update the UI to reflect the new limit.
        labelBatteryTitle.Text = $"Battery Charge Limit: {limit}%";
        trackBattery.Value = limit;

        try
        {
            // Attempt to set the battery charge limit via WMI.
            Program.WMI.DeviceSet(AsusWMI.BatteryLimit, limit);
        }
        catch
        {
            // Log failure to set the battery charge limit.
            Logger.WriteLine("Can't set battery charge limit");
        }

        // Update the configuration with the new limit.
        Program.Config.SetConfig("charge_limit", limit);
    }

    /// <summary>
    /// Handles the track bar value change event to set the battery charge limit.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The event data.</param>
    private void TrackBatteryChange(object? sender, EventArgs e)
    {
        if (sender is TrackBar bar) // Directly cast and check for null in one step.
        {
            SetBatteryChargeLimit(bar.Value);
        }
    }

    /// <summary>
    /// Handles changes in the GPU auto-configuration checkbox, updating the configuration accordingly.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The event data.</param>
    private void CheckGPU_CheckedChanged(object? sender, EventArgs e)
    {
        if (sender is CheckBox chk) // Directly cast and check for null in one step.
        {
            Program.Config.SetConfig("gpu_auto", chk.Checked ? 1 : 0);
        }
    }

    /// <summary>
    /// Handles changes in the screen auto-configuration checkbox, updating the configuration accordingly.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The event data.</param>
    private void checkScreen_CheckedChanged(object? sender, EventArgs e)
    {
        if (sender is CheckBox chk) // Directly cast and check for null in one step.
        {
            Program.Config.SetConfig("screen_auto", chk.Checked ? 1 : 0);
        }
    }
}