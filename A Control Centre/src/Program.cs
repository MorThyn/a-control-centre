using System.Diagnostics;
using System.Management;
using A_Control_Centre.form;
using A_Control_Centre.infrastructure;
using A_Control_Centre.Properties;
using A_Control_Centre.utilities;
using Microsoft.Win32;

namespace A_Control_Centre;

static class Program
{
    internal static readonly NotifyIcon trayIcon = new()
    {
        Text = "A Control Centre",
        Icon = Resources.trayIcon,
        Visible = true
    };

    internal static readonly AsusWMI WMI = new ();
    internal static readonly AppConfig Config = new ();
    internal static readonly SettingsForm SettingsForm = new ();
    internal static readonly HardwareMonitor HWMonitor = new ();

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    public static void Main()
    {
        CheckRunning();
        
        InitializeTrayIcon();
        SubscribeToSystemEvents();

        InitializeSettingsForm();
        UpdateSettingsFormBasedOnPowerStatus();
        
        // Check if the current working directory of the application, with any trailing backslashes removed,
        // is equal to the path where the application executable is located, also with any trailing backslashes removed.
        if (Environment.CurrentDirectory.Trim('\\') == Application.StartupPath.Trim('\\'))
        {
            // If the current directory is the same as the startup path,
            // call the method to toggle the visibility of the settings window/form.
            ToggleSettingsVisibility();
        }

        Application.Run();
    }

    private static void CheckRunning()
    {
        if (Process.GetProcesses().Count(p => p.ProcessName == "A Control Cente") <= 1) return;
        MessageBox.Show("A Control Centre is already running. Check system tray for an icon.", "App already running", MessageBoxButtons.OK);
        Application.Exit();
    }

    /// <summary>
    /// Initializes the tray icon and subscribes to the mouse click event.
    /// </summary>
    private static void InitializeTrayIcon()
    {
        trayIcon.MouseClick += TrayIcon_MouseClick;
        // Set the icon for trayIcon here if necessary.
        Application.ApplicationExit += OnExit;
    }

    /// <summary>
    /// Subscribes to system-wide and WMI events.
    /// </summary>
    private static void SubscribeToSystemEvents()
    {
        AsusWMI.SubscribeToEvents(WatcherEventArrived);
        SystemEvents.PowerModeChanged += SystemEvents_PowerModeChanged;
    }

    /// <summary>
    /// Initializes settings form with configuration values and hardware monitoring.
    /// </summary>
    private static void InitializeSettingsForm()
    {
        SettingsForm.InitGPUMode();
        SettingsForm.InitBoost();
        SettingsForm.InitBacklight();

        // Load configuration settings into the form.
        SettingsForm.SetPerformanceMode((AsusWMI.FanModes)Config.GetConfig("performance_mode"));
        SettingsForm.SetBatteryChargeLimit(Config.GetConfig("charge_limit"));
        SettingsForm.VisualiseGPUAuto(Config.GetConfig("gpu_auto"));
        SettingsForm.VisualiseScreenAuto(Config.GetConfig("screen_auto"));
        SettingsForm.SetStartupCheck(Startup.IsScheduled());
    }

    /// <summary>
    /// Updates settings based on the current power status.
    /// </summary>
    private static void UpdateSettingsFormBasedOnPowerStatus()
    {
        bool isPlugged = SystemInformation.PowerStatus.PowerLineStatus == PowerLineStatus.Online;
        SettingsForm.AutoScreen(isPlugged);
        SettingsForm.AutoGPUMode(isPlugged);
    }

    /// <summary>
    /// Handles the system power mode changed event.
    /// </summary>
    private static void SystemEvents_PowerModeChanged(object sender, PowerModeChangedEventArgs e)
    {
        SettingsForm.SetBatteryChargeLimit(Config.GetConfig("charge_limit"));
    }

    /// <summary>
    /// Handles WMI event arrivals and updates the settings form accordingly.
    /// </summary>
    private static void WatcherEventArrived(object sender, EventArrivedEventArgs e)
    {
        if (e.NewEvent is null) return;

        int eventId = int.Parse(e.NewEvent["EventID"].ToString());
        Debug.WriteLine(eventId);

        switch (eventId)
        {
            case 56: // Rog button
            case 87: // Battery
                SettingsForm.BeginInvoke((MethodInvoker)delegate
                {
                    SettingsForm.AutoGPUMode(false);
                    SettingsForm.AutoScreen(false);
                });
                break;
            case 88: // Plugged in
                SettingsForm.BeginInvoke((MethodInvoker)delegate
                {
                    SettingsForm.AutoGPUMode();
                    SettingsForm.AutoScreen();
                });
                break;
            case 174: // FN+F5
                SettingsForm.BeginInvoke((MethodInvoker)delegate { SettingsForm.CyclePerformanceMode(); });
                break;
            case 179: // FN+F4
                SettingsForm.BeginInvoke(delegate { SettingsForm.CycleBacklightMode(); });
                break;
        }
    }/// <summary>
    /// Toggles the visibility of the Settings Form. If the form is currently visible, it hides it.
    /// Otherwise, it shows and activates the form. Additionally, it applies the GPU mode configuration
    /// based on the saved configuration.
    /// </summary>
    static void ToggleSettingsVisibility()
    {
        // Check if the SettingsForm is currently visible.
        if (SettingsForm.Visible)
        {
            // If the form is visible, hide it.
            SettingsForm.Hide();
        }
        else
        {
            // If the form is not visible, show and activate it.
            SettingsForm.Show();
            SettingsForm.Activate();
        }

        // Apply the GPU mode configuration based on the saved settings.
        SettingsForm.VisualiseGPUMode((AsusWMI.GPUModes)Config.GetConfig("gpu_mode"));
    }

    /// <summary>
    /// Handles tray icon mouse click events to toggle the settings form visibility.
    /// </summary>
    private static void TrayIcon_MouseClick(object sender, MouseEventArgs e)
    {
        if (e.Button != MouseButtons.Left && e.Button != MouseButtons.Right) return;
        ToggleSettingsVisibility();

        // The icon needs to be refreshed as it becomes blurred when the screen resolution changes.
        trayIcon.Icon = trayIcon.Icon;
    }

    /// <summary>
    /// Cleans up resources and exits the application.
    /// </summary>
    private static void OnExit(object sender, EventArgs e)
    {
        trayIcon.Visible = false;
        Application.Exit();
    }
}