﻿using System.Diagnostics;
using System.Security.Principal;
using Microsoft.Win32.TaskScheduler;

namespace A_Control_Centre.utilities;

/// <summary>
/// Provides functionality to manage a specific scheduled task.
/// </summary>
public static class Startup
{
    private const string TaskName = "A Control Centre";

    /// <summary>
    /// Checks if the startup task is scheduled.
    /// </summary>
    /// <returns>true if the task is scheduled; otherwise, false.</returns>
    public static bool IsScheduled()
    {
        using TaskService taskService = new TaskService();
        return taskService.RootFolder.AllTasks.Any(t => t.Name == TaskName);
    }

    /// <summary>
    /// Schedules the startup task to run at user logon.
    /// </summary>
    public static void Schedule()
    {
        string executablePath = Application.ExecutablePath;

        if (string.IsNullOrEmpty(executablePath))
        {
            Logger.WriteLine($"Executable path is null or empty: {executablePath}");
            return;
        }

        string userId = WindowsIdentity.GetCurrent().Name;

        using TaskService ts = new TaskService();
        TaskDefinition td = ts.NewTask();
        td.RegistrationInfo.Description = "A Control Center Auto Start";

        // Trigger the task at user logon
        td.Triggers.Add(new LogonTrigger { UserId = userId });

        // Action to execute
        td.Actions.Add(new ExecAction(executablePath));

        // Configure task settings
        td.Settings.StopIfGoingOnBatteries = false;
        td.Settings.DisallowStartIfOnBatteries = false;

        // Register the task in the root folder
        ts.RootFolder.RegisterTaskDefinition(TaskName, td);
    }

    /// <summary>
    /// Unschedules the startup task.
    /// </summary>
    public static void UnSchedule()
    {
        using TaskService taskService = new TaskService();
        taskService.RootFolder.DeleteTask(TaskName, false);
    }
}