using System.Text.Json;

namespace A_Control_Centre.utilities;

/// <summary>
/// Manages application configuration settings by storing them in a JSON file within the user's application data folder.
/// </summary>
public class AppConfig
{
    private readonly string appPath;
    private readonly string configFile;
    private Dictionary<string, object> config;

    /// <summary>
    /// Initializes a new instance of the AppConfig class and loads the configuration from a file, or creates a default configuration if the file does not exist.
    /// </summary>
    public AppConfig()
    {
        appPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
            "A Control Centre");
        configFile = Path.Combine(appPath, "config.json");

        EnsureDirectoryExists(appPath);
        LoadOrCreateConfig();
    }

    /// <summary>
    /// Ensures that the application configuration directory exists.
    /// </summary>
    /// <param name="path">The path to the application configuration directory.</param>
    private static void EnsureDirectoryExists(string path)
    {
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
    }

    /// <summary>
    /// Loads the configuration from a file, or creates a default configuration if the file does not exist.
    /// </summary>
    private void LoadOrCreateConfig()
    {
        if (File.Exists(configFile))
        {
            try
            {
                string text = File.ReadAllText(configFile);
                config = JsonSerializer.Deserialize<Dictionary<string, object>>(text);
            }
            catch
            {
                // Log or handle the error as appropriate.
                CreateDefaultConfig();
            }
        }
        else
        {
            CreateDefaultConfig();
        }
    }

    /// <summary>
    /// Creates a default configuration and saves it to the configuration file.
    /// </summary>
    private void CreateDefaultConfig()
    {
        config = new Dictionary<string, object> { ["performance_mode"] = 0 };
        SaveConfig();
    }

    /// <summary>
    /// Saves the current configuration to the configuration file.
    /// </summary>
    private void SaveConfig()
    {
        string jsonString = JsonSerializer.Serialize(config, new JsonSerializerOptions { WriteIndented = true });
        File.WriteAllText(configFile, jsonString);
    }

    /// <summary>
    /// Retrieves an integer configuration value for a given key using TryGetValue.
    /// Returns a default value if the key is not found or if the value cannot be parsed to an integer.
    /// </summary>
    /// <param name="name">The configuration key to look up.</param>
    /// <param name="defaultValue">The default value to return if the key is not found or if parsing fails. Defaults to -1.</param>
    /// <returns>The configuration value if found and successfully parsed; otherwise, the specified default value.</returns>
    public int GetConfig(string name, int defaultValue = -1)
    {
        // Attempt to retrieve the value from the dictionary.
        if (!config.TryGetValue(name, out var value)) return defaultValue;
        // If the value is successfully retrieved, attempt to parse it.
        return int.TryParse(value.ToString(), out int intValue) ? intValue :
            // Return the default value if the key is not found or if parsing fails.
            defaultValue;
    }

    /// <summary>
    /// Sets an integer configuration value by name and saves the configuration.
    /// </summary>
    /// <param name="name">The name of the configuration setting.</param>
    /// <param name="value">The integer value to set.</param>
    public void SetConfig(string name, int value)
    {
        config[name] = value;
        SaveConfig();
    }

    /// <summary>
    /// Sets a string configuration value by name and saves the configuration.
    /// </summary>
    /// <param name="name">The name of the configuration setting.</param>
    /// <param name="value">The string value to set.</param>
    public void SetConfig(string name, string value)
    {
        config[name] = value;
        SaveConfig();
    }
}