﻿using System.Diagnostics;

namespace A_Control_Centre.utilities;

/// <summary>
/// Provides logging functionality to write messages with a timestamp to the debug output and a log file.
/// </summary>
public static class Logger
{
    private static readonly string AppPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "A Control Centre");
    private static readonly string LogFile = Path.Combine(AppPath, "log.txt");

    /// <summary>
    /// Writes a log message with a timestamp to the debug output and to a log file in the application data folder.
    /// </summary>
    /// <param name="logMessage">The message to log.</param>
    public static void WriteLine(string logMessage)
    {
        // Write the log message to the Debug output.
        Debug.WriteLine(logMessage);

        // Ensure the log directory exists.
        EnsureLogDirectoryExists();

        // Append the log message to the log file.
        AppendLogMessageToFile(logMessage);
    }

    /// <summary>
    /// Ensures the log directory exists, and if not, creates it.
    /// </summary>
    private static void EnsureLogDirectoryExists()
    {
        if (!Directory.Exists(AppPath))
        {
            Directory.CreateDirectory(AppPath);
        }
    }

    /// <summary>
    /// Appends a log message to the log file with a timestamp.
    /// </summary>
    /// <param name="logMessage">The message to append to the log file.</param>
    private static void AppendLogMessageToFile(string logMessage)
    {
        using StreamWriter writer = File.AppendText(LogFile);
        writer.WriteLine($"{DateTime.Now}: {logMessage}");
    }
}