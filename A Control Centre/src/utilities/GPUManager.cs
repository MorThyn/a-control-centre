﻿using System.Runtime.InteropServices;
using A_Control_Centre.infrastructure;

namespace A_Control_Centre.utilities;

//Courtesy of https://github.com/GPUOpen-LibrariesAndSDKs/display-library/blob/master/Sample-Managed/Program.cs

public class GPUManager : IDisposable
{
    private bool _isReady;
    private IntPtr _adlContextHandle = IntPtr.Zero;
    private ADLAdapterInfo _internalDiscreteAdapter;

    /// <summary>
    /// Initializes a new instance of the GpuTemperatureProvider class.
    /// </summary>
    public GPUManager()
    {
        InitializeAdl();
    }

    /// <summary>
    /// Attempts to retrieve the current temperature of the GPU.
    /// </summary>
    /// <returns>The current temperature if available; otherwise, null.</returns>
    public int? GetCurrentTemperature()
    {
        if (AMDDisplayLibrary.AMDNativeMethods.ADL2_New_QueryPMLogData_Get(_adlContextHandle, _internalDiscreteAdapter.AdapterIndex, out ADLPMLogDataOutput adlpmLogDataOutput) != AMDDisplayLibrary.ADL_SUCCESS)
            return null;

        ADLSingleSensorData temperatureSensor = adlpmLogDataOutput.Sensors[(int)ADLSensorType.PMLOG_TEMPERATURE_EDGE];
        return temperatureSensor.Supported != 0 ? temperatureSensor.Value : (int?)null;
    }

    private void InitializeAdl()
    {
        if (!AMDDisplayLibrary.Load() || AMDDisplayLibrary.ADL2_Main_Control_Create(1, out _adlContextHandle) != AMDDisplayLibrary.ADL_SUCCESS)
            return;

        if (!TryGetInternalDiscreteAdapter(out _internalDiscreteAdapter))
            return;

        _isReady = true;
    }

    private bool TryGetInternalDiscreteAdapter(out ADLAdapterInfo adapterInfo)
    {
        adapterInfo = default;
        AMDDisplayLibrary.AMDNativeMethods.ADL2_Adapter_NumberOfAdapters_Get(_adlContextHandle, out int numberOfAdapters);
        if (numberOfAdapters <= 0)
            return false;

        ADLAdapterInfoArray osAdapterInfoData = new ADLAdapterInfoArray();
        int osAdapterInfoDataSize = Marshal.SizeOf(osAdapterInfoData);
        IntPtr adapterBuffer = Marshal.AllocCoTaskMem(osAdapterInfoDataSize);

        try
        {
            Marshal.StructureToPtr(osAdapterInfoData, adapterBuffer, false);
            if (AMDDisplayLibrary.AMDNativeMethods.ADL2_Adapter_AdapterInfo_Get(_adlContextHandle, adapterBuffer, osAdapterInfoDataSize) != AMDDisplayLibrary.ADL_SUCCESS)
                return false;

            osAdapterInfoData = (ADLAdapterInfoArray)Marshal.PtrToStructure(adapterBuffer, typeof(ADLAdapterInfoArray))!;
            const int amdVendorId = 1002;

            // Determine which GPU is internal discrete AMD GPU
            adapterInfo = osAdapterInfoData.ADLAdapterInfo.FirstOrDefault(IsInternalDiscreteAmdGpu);

            return adapterInfo.Exist != 0;

            bool IsInternalDiscreteAmdGpu(ADLAdapterInfo adapter) =>
                adapter.Exist != 0 && adapter.Present != 0 &&
                adapter.VendorID == amdVendorId &&
                AMDDisplayLibrary.AMDNativeMethods.ADL2_Adapter_ASICFamilyType_Get(_adlContextHandle, adapter.AdapterIndex, out ADLAsicFamilyType asicFamilyType, out int asicFamilyTypeValids) == AMDDisplayLibrary.ADL_SUCCESS &&
                ((asicFamilyType & (ADLAsicFamilyType)((int)asicFamilyType & asicFamilyTypeValids) & ADLAsicFamilyType.Discrete) != 0);
        }
        finally
        {
            Marshal.FreeCoTaskMem(adapterBuffer);
        }
    }

    private void ReleaseUnmanagedResources()
    {
        if (_adlContextHandle == IntPtr.Zero) return;
        AMDDisplayLibrary.AMDNativeMethods.ADL2_Main_Control_Destroy(_adlContextHandle);
        _adlContextHandle = IntPtr.Zero;
        _isReady = false;
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
        ReleaseUnmanagedResources();
        GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Finalizes an instance of the GpuTemperatureProvider class.
    /// </summary>
    ~GPUManager()
    {
        ReleaseUnmanagedResources();
    }
}